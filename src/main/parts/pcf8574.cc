/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "parts/pcf8574.h"

#include <stdexcept> // C++ standard exceptions


namespace ModuCab { namespace Parts {

using ::ModuCab::Interfaces::I2C::IBus;

PCF8574::PCF8574(IBus& bus, byte_t address, PCF8574::Mode mode):
	bus(bus),
	address(address & 0x7F),
	mode(mode)
{} // constructor(IBus, byte_t, Mode)

PCF8574::PCF8574(IBus& bus, bool versionA, byte_t address, PCF8574::Mode mode):
	PCF8574(bus, ((versionA ? 0b0111 : 0b0100) << 3) | (address & 0b111), mode)
{} // constructor(IBus, bool, byte_t, Mode)

PCF8574::Mode PCF8574::getMode(){
	return this->mode;
} // getMode

bool PCF8574::in(byte_t* data, const unsigned char count){
	// check mode
	if(!(static_cast<unsigned char>(this->mode) & static_cast<unsigned char>(PCF8574::Mode::Input))){
		throw ::std::logic_error("Tried to read inputs but initialized for output mode only!");
	};
	
	return (this->bus.read(this->address, data, count) == count);
} // in

bool PCF8574::out(const byte_t data){
	// check mode
	if(!(static_cast<unsigned char>(this->mode) & static_cast<unsigned char>(PCF8574::Mode::Output))){
		throw ::std::logic_error("Tried to write outputs but initialized for input mode only!");
	};
	
	return (this->bus.write_8(this->address, data) == 1);
} // out

}} // namespace ModuCab::Parts
