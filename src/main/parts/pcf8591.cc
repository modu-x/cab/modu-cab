/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "parts/pcf8591.h"


namespace ModuCab { namespace Parts {

/**
 * Returns control byte
 * @param adc_mode - analog input programming
 * @param adc_channel - channel to read from
 * @param dac_on - turn DAC output on when true
 * @return byte
 */
byte_t formatControlByte(PCF8591::Inputs adc_mode, PCF8591::Channel adc_channel, bool dac_on){
	return (dac_on ? (1 << 6) : 0)
		| (static_cast<unsigned char>(adc_mode) << 4)
		| ((adc_channel == PCF8591::Channel::All) ? (1 << 2) : 0)
		| (static_cast<unsigned char>(adc_channel) & 0b11)
	;
} // formatControlByte


PCF8591::PCF8591(::ModuCab::Interfaces::I2C::IBus& bus, byte_t address, bool addr_pins):
	bus(bus),
	address(addr_pins ? ((0b1001 << 3) | (address & 0b111)) : (address & 0x7F))
{
	// synchronize expander to defaults
	this->dac(false);// enough as other options are also sent
} // constructor

bool PCF8591::adc(PCF8591::Inputs mode, PCF8591::Channel channel, byte_t* data, const unsigned char count){
	return this->adc(mode, channel) && this->adc(data, count);
} // adc(Inputs, Channel, byte_t*, count)

bool PCF8591::adc(PCF8591::Channel channel, byte_t* data, const unsigned char count){
	return this->adc(channel) && this->adc(data, count);
} // adc(Channel, byte_t*, count)

bool PCF8591::adc(byte_t* data, const unsigned char count){
	// flush previous conversion result
	byte_t trash[4];
	unsigned char length = (this->adc_channel == PCF8591::Channel::All && this->last_control) ? 4 : 1;
	if(this->bus.read(this->address, trash, length) != length){
		return false;
	};
	this->last_control = false;
	
	// read data
	if(this->bus.read(this->address, data, count) != count){
		return false;
	};
	
	// flush rest of data if all channels selected but only some read
	length = count % 4;
	if(this->adc_channel == PCF8591::Channel::All && length > 0){
		if(this->bus.read(this->address, trash, length) != length){
			return false;
		};
	};
	
	return true;
} // adc(byte_t*, count)

bool PCF8591::adc(PCF8591::Inputs mode, PCF8591::Channel channel){
	this->last_control = true;
	if(this->bus.write_8(this->address, formatControlByte(mode, channel, this->dac_on)) != 1){
		return false;
	};
	
	this->adc_mode = mode;
	this->adc_channel = channel;
	
	return true;
} // adc(Inputs, Channel)

bool PCF8591::adc(PCF8591::Channel channel){
	this->last_control = true;
	if(this->bus.write_8(this->address, formatControlByte(this->adc_mode, channel, this->dac_on)) != 1){
		return false;
	};
	
	this->adc_channel = channel;
	
	return true;
} // adc(Channel)

bool PCF8591::dac(bool on, const byte_t value){
	this->last_control = true;
	if(this->bus.write_16(this->address, formatControlByte(this->adc_mode, this->adc_channel, on), value) != 2){
		return false;
	};
	
	this->dac_on = on;
	
	return true;
} // dac(bool, byte_t)

bool PCF8591::dac(bool on){
	this->last_control = true;
	if(this->bus.write_8(this->address, formatControlByte(this->adc_mode, this->adc_channel, on)) != 1){
		return false;
	};
	
	this->dac_on = on;
	
	return true;
} // dac(bool)

}} // namespace ModuCab::Parts
