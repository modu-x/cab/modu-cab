/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Parts
 * PCF8591 I2C 4xADC + DAC expander
 */
#ifndef MODUCAB_PARTS_PCF8591_H_
#define MODUCAB_PARTS_PCF8591_H_


#include "interfaces/i2c/i_bus.h"
#include "utils/types.h"


namespace ModuCab { namespace Parts {

class PCF8591 {
	public:
		/**
		 * ADC channels
		 */
		enum class Channel : unsigned char {
			AIN0 = 0,
			AIN1,
			AIN2,
			AIN3,
			
			All = 128
		}; // enum Channel
		
		/**
		 * Analog inputs modes
		 */
		enum class Inputs : unsigned char {
			/** Four single-ended inputs */
			Single = 0,
			
			/** Three differential inputs (0..2 against 3) */
			Differential3 = 1,
			
			/** Single-ended and differential mixed (0..1 single, 2..3 differential) */
			Mixed = 2,
			
			/** Two differential inputs (0..1, 2..3) */
			Differential = 3
		}; // enum Inputs
		
		
		/**
		 * Constructor
		 * @param bus - reference to I2C bus
		 * @param address - expander address (7-bit) or state of A2..A0 pins according to addr_pins parameter
		 * @param addr_pins - use address parameter as state of A2..A0 pins when true
		 */
		PCF8591(::ModuCab::Interfaces::I2C::IBus& bus, byte_t address, bool addr_pins = false);
		
		
		/**
		 * Reads ADC data from expander
		 * @param mode - analog input programming
		 * @param channel - channel to read from
		 * @param data - pointer to store data at
		 * @param count - number of data bytes to read
		 * @return success/failure
		 */
		bool adc(Inputs mode, Channel channel, byte_t* data, const unsigned char count);
		bool adc(Channel channel, byte_t* data, const unsigned char count);
		bool adc(byte_t* data, const unsigned char count);
		bool adc(Inputs mode, Channel channel);
		bool adc(Channel channel);
		
		/**
		 * Sends DAC data to expander
		 * @param on - turn DAC output on when true
		 * @param value
		 * @return success/failure
		 */
		bool dac(bool on, const byte_t value);
		bool dac(bool on);
	
	
	private:
		/** Reference to I2C bus */
		::ModuCab::Interfaces::I2C::IBus& bus;
		
		/** Expander 7-bit address */
		byte_t address;
		
		/** ADC input channel */
		Channel adc_channel = Channel::AIN0;
		
		/** ADC inputs mode */
		Inputs adc_mode = Inputs::Single;
		
		/** DAC output enabled */
		bool dac_on = false;
		
		/** Last communicated byte was a control byte - allows correct ADC channel synchronization */
		bool last_control = false;
}; // class PCF8591

}} // namespace ModuCab::Parts


#endif
