/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Parts
 * PCF8574 I2C 8-bit I/O expander
 */
#ifndef MODUCAB_PARTS_PCF8574_H_
#define MODUCAB_PARTS_PCF8574_H_


#include "interfaces/i2c/i_bus.h"
#include "utils/types.h"


namespace ModuCab { namespace Parts {

class PCF8574 {
	public:
		/**
		 * Modes of expander
		 */
		enum class Mode : unsigned char {
			Input = 0b01,
			Output = 0b10,
			InOut = 0b11
		}; // enum Mode
		
		
		/**
		 * Constructor
		 * @param bus - reference to I2C bus
		 * @param address - expander address (7-bit)
		 * @param mode - Input/Output mode
		 */
		PCF8574(::ModuCab::Interfaces::I2C::IBus& bus, byte_t address, Mode mode);
		
		/**
		 * Constructor
		 * @param bus - reference to I2C bus
		 * @param versionA - true for PCF8574A chip
		 * @param address - expander address defined by its A2..A0 pins
		 * @param mode - Input/Output mode
		 */
		PCF8574(::ModuCab::Interfaces::I2C::IBus& bus, bool versionA, byte_t address, Mode mode);
		
		
		/**
		 * Returns expander mode
		 * @return mode
		 */
		Mode getMode();
		
		/**
		 * Reads data from expander
		 * @param data - pointer to store data at
		 * @param count - number of data bytes to read
		 * @return success/failure
		 */
		bool in(byte_t* data, const unsigned char count);
		
		/**
		 * Sends data to expander
		 * @param data
		 * @return success/failure
		 */
		bool out(const byte_t data);
	
	
	private:
		/** Reference to I2C bus */
		::ModuCab::Interfaces::I2C::IBus& bus;
		
		/** Expander 7-bit address */
		byte_t address;
		
		/** Expander mode */
		Mode mode;
}; // class PCF8574

}} // namespace ModuCab::Parts


#endif
