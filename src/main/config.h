/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab
 * Compile-time configuration (dependency for macros, etc.)
 */
#ifndef MODUCAB_CONFIG_H_
#define MODUCAB_CONFIG_H_


/**
 * Enable multi-threading
 */
#define THREADING


#endif
