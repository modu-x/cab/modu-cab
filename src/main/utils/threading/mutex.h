/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Utils::Threading
 * Macros for mutexes - enabling/disabling it in one place
 */
#ifndef MODUCAB_UTILS_THREADING_MUTEX_H_
#define MODUCAB_UTILS_THREADING_MUTEX_H_


#include "config.h"


#ifdef THREADING
	#include <mutex>
	
	#define MUTEX(mtx) ::std::mutex mtx;
	#define MUTEX_RECURSIVE(mtx) ::std::recursive_mutex mtx;
	#define MUTEX_LOCK(mtx) mtx.lock();
	#define MUTEX_TRY_LOCK(mtx) mtx.try_lock();
	#define MUTEX_UNLOCK(mtx) mtx.unlock();
	#define MUTEX_THIS_LOCK(mtx) this->mtx.lock();
	#define MUTEX_THIS_TRY_LOCK(mtx) this->mtx.try_lock();
	#define MUTEX_THIS_UNLOCK(mtx) this->mtx.unlock();
#else
	#define MUTEX(mtx)
	#define MUTEX_RECURSIVE(mtx)
	#define MUTEX_LOCK(mtx)
	#define MUTEX_TRY_LOCK(mtx)
	#define MUTEX_UNLOCK(mtx)
	#define MUTEX_THIS_LOCK(mtx)
	#define MUTEX_THIS_TRY_LOCK(mtx)
	#define MUTEX_THIS_UNLOCK(mtx)
#endif


#endif
