/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab
 * Types definitions
 */
#ifndef MODUCAB_UTILS_TYPES_H_
#define MODUCAB_UTILS_TYPES_H_


#include <climits>
#include <cstdint>


namespace ModuCab {

/**
 * Type to store 8bit values - mainly for manipulation with core data arrays
 */
typedef ::std::uint8_t byte_t;
//typedef ::std::uint_least8_t byte_t;

/**
 * Type to store length of byte_t arrays
 * This type have to be always unsigned as other code depends on that.
 */
typedef unsigned int length_t;
/** Its maximum value */
#define LENGTH_MAX UINT_MAX

} // namespace ModuCab


#endif
