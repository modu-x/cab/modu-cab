/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab
 * Core types definitions
 */
#ifndef MODUCAB_UTILS_TYPESCORE_H_
#define MODUCAB_UTILS_TYPESCORE_H_


#include <climits>

#include "core/i_device.h"


namespace ModuCab {

/**
 * Specify a part of array by offset and length
 */
struct arraypart_t {
	unsigned int offset;
	unsigned int length;
}; // arraypart_t

/**
 * Structure for storing devices in an array
 */
struct device_t {
	Core::IDevice& device;
	arraypart_t data_down;
	arraypart_t data_up;
}; // device_t

/**
 * Type to store length of device_t arrays
 * This type have to be always unsigned as other code depends on that.
 */
typedef unsigned int devlength_t;
/** Its maximum value */
#define DEVLENGTH_MAX UINT_MAX

} // namespace ModuCab


#endif
