/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Utils::Checking
 * Miscellaneous checking functions
 */
#ifndef MODUCAB_UTILS_CHECKING_H_
#define MODUCAB_UTILS_CHECKING_H_


#include "utils/types.h"


namespace ModuCab { namespace Utils { namespace Checking {

/** Error codes */
static const int BAD_DATA_POINTER = -1;
static const int NO_DATA = -2;
static const int DATA_OFFSET_OUT_OF_BOUNDS = -3;
static const int DATA_LENGTH_OUT_OF_BOUNDS = -4;


/**
 * Checks validity of length and offset request
 * @param available - available length of data
 * @param length
 * @param offset
 * @return zero when OK, negative on error
 */
inline int checkLength(length_t available, length_t length, length_t offset){
	if(available < 1){
		return NO_DATA;
	}
	else if(available <= offset){
		return DATA_OFFSET_OUT_OF_BOUNDS;
	}
	else if(available - offset < length){
		return DATA_LENGTH_OUT_OF_BOUNDS;
	}
	else {
		return 0;
	};
} // checkLength

/**
 * Checks validity of length and offset request & buffer pointer
 * @param buff - pointer to data buffer
 * @param available - available length of data
 * @param length
 * @param offset
 * @return zero when OK, negative on error
 */
inline int checkBufferLength(const byte_t* buff, length_t available, length_t length, length_t offset){
	if(buff == nullptr){
		return BAD_DATA_POINTER;
	}
	else {
		return checkLength(available, length, offset);
	};
} // checkBufferLength

}}} // namespace ModuCab::Utils::Checking


#endif
