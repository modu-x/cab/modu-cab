/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Utils
 * Simple class for translating calls to allow the same interface on more "sides"
 */
#ifndef MODUCAB_UTILS_INTERFACECALLER_H_
#define MODUCAB_UTILS_INTERFACECALLER_H_


#include "interfaces/i_generic.h"


namespace ModuCab { namespace Utils {

template <typename C>
class InterfaceCaller: public ::ModuCab::Interfaces::IGeneric {
	public:
		/**
		 * Constructor
		 * @param cls - reference to underlying class the functions belongs to
		 * @param f_data - pointer to underlying function for IGeneric::data
		 * @param f_dataRequest - pointer to underlying function for IGeneric::dataRequest(byte_t*, length_t, length_t)
		 * @param f_dataRequestCall - pointer to underlying function for IGeneric::dataRequest(length_t, length_t)
		 */
		InterfaceCaller(
			C& cls,
			int (C::*f_data)(const byte_t*, length_t, length_t),
			int (C::*f_dataRequest)(byte_t*, length_t, length_t),
			int (C::*f_dataRequestCall)(length_t, length_t)
		):
			cls(cls),
			f_data(f_data),
			f_dataRequest(f_dataRequest),
			f_dataRequestCall(f_dataRequestCall)
		{};
		
		
		/**
		 * IGeneric
		 */
		inline int data(const byte_t* data, length_t length, length_t offset) override {
			return (this->cls .* this->f_data)(data, length, offset);
		} // data
		
		inline int dataRequest(byte_t* data, length_t length, length_t offset) override {
			return (this->cls .* this->f_dataRequest)(data, length, offset);
		} // dataRequest(byte_t*, length_t, length_t)
		
		inline int dataRequest(length_t length, length_t offset) override {
			return (this->cls .* this->f_dataRequestCall)(length, offset);
		} // dataRequest(length_t, length_t)
	
	
	private:
		/** Reference to underlying class to call functions on */
		C& cls;
		
		/** Pointers to underlying functions */
		int (C::*f_data)(const byte_t*, length_t, length_t);
		int (C::*f_dataRequest)(byte_t*, length_t, length_t);
		int (C::*f_dataRequestCall)(length_t, length_t);
}; // class InterfaceCaller

}} // namespace ModuCab::Utils


#endif
