/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Core
 * Core interface for downstream
 */
#ifndef MODUCAB_CORE_ICOREDOWNSTREAM_H_
#define MODUCAB_CORE_ICOREDOWNSTREAM_H_


#include "core/i_device.h"
#include "utils/types.h"


namespace ModuCab { namespace Core {

class ICoreDownstream {
	public:
		/** Error codes */
		static const int BAD_DATA_POINTER = -1;
		static const int NO_DATA = -2;// there is no input/output data buffer known to core for this device
		static const int DATA_OFFSET_OUT_OF_BOUNDS = -3;
		static const int DATA_LENGTH_OUT_OF_BOUNDS = -4;
		static const int BAD_DEVICE_POINTER = -10;
		
		
		/**
		 * Data from device to core (input)
		 * @param device - the instance, which sends the data
		 * @param data - pointer to byte array
		 * @param length - number of bytes
		 * @param offset - byte offset of data in device array
		 * @return number of bytes received or negative as error code
		 */
		virtual int devData(IDevice& device, const byte_t* data, length_t length, length_t offset = 0) =0;
		
		/**
		 * Request for data from core to device (output)
		 * @param device - the instance, which requests the data
		 * @param length - number of bytes
		 * @param offset - byte offset of data in device array
		 * @return number of bytes passed or negative as error code
		 *
		 * The core responds to this request by subsequent call to device's coreData(...) method.
		 * If multi-threaded, the method will be called from the same thread, so be aware of not creating deadlock situation.
		 */
		virtual int devRequest(IDevice& device, length_t length, length_t offset = 0) =0;
		
		/**
		 * Request for data from core to device (output)
		 * @param device - the instance, which requests the data
		 * @param data - pointer to byte array, where the device wants the core to store the requested data
		 * @param length - number of bytes
		 * @param offset - byte offset of data in device array
		 * @return number of bytes passed or negative as error code
		 *
		 * The core will put the requested data into the pointed array.
		 * If multi-threaded, the device must take care about possible locking/unlocking the data-mutex before/after call to this method.
		 */
		virtual int devRequest(IDevice& device, byte_t* data, length_t length, length_t offset = 0) =0;
}; // class ICoreDownstream

}} // namespace ModuCab::Core


#endif
