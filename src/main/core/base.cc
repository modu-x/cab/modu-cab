/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "core/base.h"

#include "config.h"


namespace ModuCab { namespace Core {

Base::Base(byte_t* data_in, byte_t* data_out, const device_t* devices, devlength_t devices_length):
	data_in(data_in),
	data_out(data_out),
	devices(devices),
	devices_length(devices_length)
{}// constructor

int Base::devData(IDevice* device, const byte_t* data, length_t length, length_t offset){
	// check length
	if(!length){
		return 0;
	};
	
	// check null pointers
	if(device == nullptr){
		return ICoreDownstream::BAD_DEVICE_POINTER;
	}
	else if(data == nullptr){
		return ICoreDownstream::BAD_DATA_POINTER;
	};
	
	// find the device in the known list
	devlength_t id = this->findDevice(device);
	if(id == DEVLENGTH_MAX){
		return ICoreDownstream::BAD_DEVICE_POINTER;
	};
	
	// check data existence, offset, length
	int ret = this->checkLength(this->devices[id].data_in.length, length, offset);
	if(ret < 0){
		return ret;
	};
	
	// compute offset to main array
	offset = this->devices[id].data_in.offset + offset;
	
	// access the buffer and copy data
	MUTEX_THIS_LOCK(data_in_lock)
	
	for(length_t i = 0; i < length; i++){
		this->data_in[offset + i] = data[i];
	};
	
	MUTEX_THIS_UNLOCK(data_in_lock)
	
	return length;
} // devData

int Base::devRequest(IDevice* device, length_t length, length_t offset){
	// check length
	if(!length){
		return 0;
	};
	
	// check null pointers
	if(device == nullptr){
		return ICoreDownstream::BAD_DEVICE_POINTER;
	};
	
	// find the device in the known list
	devlength_t id = this->findDevice(device);
	if(id == DEVLENGTH_MAX){
		return ICoreDownstream::BAD_DEVICE_POINTER;
	};
	
	// check data existence, offset, length
	int ret = this->checkLength(this->devices[id].data_out.length, length, offset);
	if(ret < 0){
		return ret;
	};
	
	#ifdef THREADING
		// create new buffer to be passed to device to allow unlocking of the core mutex as early as possible
		byte_t data[length];
		
		// compute offset to main array
		length_t offset_core = this->devices[id].data_out.offset + offset;
		
		// copy data to the buffer
		MUTEX_THIS_LOCK(data_out_lock)
		
		for(length_t i = 0; i < length; i++){
			data[i] = this->data_out[offset_core + i];
		};
		
		MUTEX_THIS_UNLOCK(data_out_lock)
		
		// pass the buffer to the device
		device->coreData(data, length, offset);
	#else
		// pass the pointer to the beginning of requested section in main array (the pointer is defined as const in IDevice)
		device->coreData(this->data_out + this->devices[id].data_out.offset + offset, length, offset);
	#endif
	
	return length;
} // devRequest(IDevice*, length_t, length_t)

int Base::devRequest(IDevice* device, byte_t* data, length_t length, length_t offset){
	// check length
	if(!length){
		return 0;
	};
	
	// check null pointers
	if(device == nullptr){
		return ICoreDownstream::BAD_DEVICE_POINTER;
	}
	else if(data == nullptr){
		return ICoreDownstream::BAD_DATA_POINTER;
	};
	
	// find the device in the known list
	devlength_t id = this->findDevice(device);
	if(id == DEVLENGTH_MAX){
		return ICoreDownstream::BAD_DEVICE_POINTER;
	};
	
	// check data existence, offset, length
	int ret = this->checkLength(this->devices[id].data_out.length, length, offset);
	if(ret < 0){
		return ret;
	};
	
	// compute offset to main array
	offset = this->devices[id].data_out.offset + offset;
	
	// access the buffer and copy data
	MUTEX_THIS_LOCK(data_out_lock)
	
	for(length_t i = 0; i < length; i++){
		data[i] = this->data_out[offset + i];
	};
	
	MUTEX_THIS_UNLOCK(data_out_lock)
	
	return length;
} // devRequest(IDevice*, byte_t*, length_t, length_t)

int Base::checkLength(length_t available, length_t length, length_t offset){
	if(!available){
		return ICoreDownstream::NO_DATA;
	}
	else if(available <= offset){
		return ICoreDownstream::DATA_OFFSET_OUT_OF_BOUNDS;
	}
	else if(available - offset < length){
		return ICoreDownstream::DATA_LENGTH_OUT_OF_BOUNDS;
	}
	else {
		return 0;
	};
} // checkLength

devlength_t Base::findDevice(IDevice* device){
	for(devlength_t i = 0; i < this->devices_length; i++){
		if(this->devices[i].device == device){
			return i;
		};
	};
	
	return DEVLENGTH_MAX;
} // findDevice

}} // namespace ModuCab::Core
