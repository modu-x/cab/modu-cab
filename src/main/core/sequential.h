/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Core
 * Sequential core with multi-thread guards only
 */
#ifndef MODUCAB_CORE_SEQUENTIAL_H_
#define MODUCAB_CORE_SEQUENTIAL_H_


#include "core/i_core_downstream.h"
#include "core/i_device.h"
#include "interfaces/i_generic.h"
#include "utils/threading/mutex.h"
#include "utils/types.h"
#include "utils/types_core.h"


namespace ModuCab { namespace Core {

class Sequential: public ICoreDownstream, public ::ModuCab::Interfaces::IGeneric {
	public:
		/**
		 * Constructor
		 * @param iface - reference to upstream interface
		 * @param data_down - downstream data buffer
		 * @param data_down_length - downstream data buffer length
		 * @param data_up - upstream data buffer
		 * @param data_up_length - upstream data buffer length
		 */
		Sequential(IGeneric& iface,
			byte_t * const data_down, length_t data_down_length,
			byte_t * const data_up, length_t data_up_length
		);
		
		/**
		 * Injects devices
		 * @param devices - array of devices; all checks, ordering, etc. must be ensured by caller
		 * @param devices_length - length of devices array
		 */
		void injectDevices(const device_t * devices, devlength_t devices_length);
		
		
		/**
		 * ICoreDownstream
		 */
		int devData(IDevice& device, const byte_t* data, length_t length, length_t offset) override;
		int devRequest(IDevice& device, length_t length, length_t offset) override;
		int devRequest(IDevice& device, byte_t* data, length_t length, length_t offset) override;
		
		
		/**
		 * IGeneric
		 */
		int data(const byte_t* data, length_t length, length_t offset) override;
		int dataRequest(byte_t* data, length_t length, length_t offset) override;
		int dataRequest(length_t length, length_t offset) override;
		
		
		/**
		 * Do one main loop cycle
		 */
		void cycle();
	
	
	protected:
		/** Mutex to block calls from IGeneric during cycle */
		MUTEX(busy)
		
		/** Reference to upstream interface */
		IGeneric& iface;
		
		/** Downstream data buffer */
		byte_t * const data_down;
		const length_t data_down_length;
		
		/** Upstream data buffer */
		byte_t * const data_up;
		const length_t data_up_length;
		
		/** Array of all known devices */
		const device_t * devices = nullptr;
		
		/** Length of the array */
		devlength_t devices_length = 0;
		
		
		/**
		 * Finds device in the array and returns its index
		 * @param device - pointer to device instance to search for
		 * @return index of device in the array or DEVLENGTH_MAX if not found (as the maximum valid index is the maximum value minus one)
		 */
		devlength_t findDevice(IDevice& device);
}; // class Sequential

}} // namespace ModuCab::Core


#endif
