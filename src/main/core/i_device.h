/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Core
 * Device interface
 */
#ifndef MODUCAB_CORE_IDEVICE_H_
#define MODUCAB_CORE_IDEVICE_H_


#include "utils/types.h"


namespace ModuCab { namespace Core {

class IDevice {
	public:
		/**
		 * Data from core to device (output)
		 * @param data - pointer to byte array
		 * @param length - number of bytes
		 * @param offset - byte offset of data in device array
		 */
		virtual void coreData(const byte_t* data, length_t length, length_t offset = 0) =0;
		
		/**
		 * Request for data from device to core (input)
		 * @param length - number of bytes
		 * @param offset - byte offset of data in device array
		 *
		 * If the device wants to respond to the request (not required but recommended), it should do that by subsequent call to core's data(...) function.
		 * If not, this function could simply return.
		 * The core does not perform any check, whether the data was updated or not - it would simply use tha data from its own buffer after this call returns.
		 * Due to that, the device could even reply with update to data not requested.
		 * It is just assumed, that the device has some good reason to do so.
		 */
		virtual void coreRequest(length_t length, length_t offset = 0) =0;
}; // class IDevice

}} // namespace ModuCab::Core


#endif
