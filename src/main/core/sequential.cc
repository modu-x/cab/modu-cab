/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "core/sequential.h"

#include "config.h"
#include "utils/checking.h"
#include "utils/types.h"
#include "utils/types_core.h"


namespace ModuCab { namespace Core {

using ::ModuCab::Utils::Checking::checkLength;
using ::ModuCab::Utils::Checking::checkBufferLength;

Sequential::Sequential(IGeneric& iface,
	byte_t * const data_down, length_t data_down_length,
	byte_t * const data_up, length_t data_up_length
):
	iface(iface),
	data_down(data_down),
	data_down_length(data_down_length),
	data_up(data_up),
	data_up_length(data_up_length)
{}// constructor

void Sequential::injectDevices(const device_t * devices, devlength_t devices_length){
	if(!this->devices){
		this->devices = devices;
		this->devices_length = devices_length;
	};
} // injectDevices

int Sequential::devData(IDevice& device, const byte_t* data, length_t length, length_t offset){
	// check length
	if(!length){
		return 0;
	};
	
	// find the device in the known list
	devlength_t id = this->findDevice(device);
	if(id == DEVLENGTH_MAX){
		return ICoreDownstream::BAD_DEVICE_POINTER;
	};
	
	// check data existence, offset, length
	int ret = checkBufferLength(data, this->devices[id].data_up.length, length, offset);
	if(ret < 0){
		return ret;
	};
	
	// compute offset to main array
	offset = this->devices[id].data_up.offset + offset;
	byte_t* data_off = this->data_up + offset;
	
	// access the buffer and copy data
	for(length_t i = 0; i < length; i++){
		data_off[i] = data[i];
	};
	
	// pass the data to upstream interface
	this->iface.data(data_off, length, offset);
	
	return length;
} // devData

int Sequential::devRequest(IDevice& device, length_t length, length_t offset){
	// check length
	if(!length){
		return 0;
	};
	
	// find the device in the known list
	devlength_t id = this->findDevice(device);
	if(id == DEVLENGTH_MAX){
		return ICoreDownstream::BAD_DEVICE_POINTER;
	};
	
	// check data existence, offset, length
	int ret = checkLength(this->devices[id].data_down.length, length, offset);
	if(ret < 0){
		return ret;
	};
	
	// pass the pointer to the beginning of requested section in main array (the pointer is defined as const in IDevice)
	device.coreData(this->data_down + this->devices[id].data_down.offset + offset, length, offset);
	
	return length;
} // devRequest(IDevice&, length_t, length_t)

int Sequential::devRequest(IDevice& device, byte_t* data, length_t length, length_t offset){
	// check length
	if(!length){
		return 0;
	};
	
	// find the device in the known list
	devlength_t id = this->findDevice(device);
	if(id == DEVLENGTH_MAX){
		return ICoreDownstream::BAD_DEVICE_POINTER;
	};
	
	// check data existence, offset, length
	int ret = checkBufferLength(data, this->devices[id].data_down.length, length, offset);
	if(ret < 0){
		return ret;
	};
	
	// compute offset to main array
	offset = this->devices[id].data_down.offset + offset;
	
	// access the buffer and copy data
	for(length_t i = 0; i < length; i++){
		data[i] = this->data_down[offset + i];
	};
	
	return length;
} // devRequest(IDevice&, byte_t*, length_t, length_t)

int Sequential::data(const byte_t* data, length_t length, length_t offset){
	// check length
	if(!length){
		return 0;
	};
	
	// check data existence, offset, length
	int ret = checkBufferLength(data, this->data_down_length, length, offset);
	if(ret < 0){
		return ret;
	};
	
	// try to acquire mutex
	#ifdef THREADING
		if(!this->busy.try_lock()){
			return 0;
		};
	#endif
	
	// access the buffer and copy data
	for(length_t i = 0; i < length; i++){
		this->data_down[offset + i] = data[i];
	};
	
	// data will be passed to devices during cycle
	
	MUTEX_THIS_UNLOCK(busy)
	
	return length;
} // data

int Sequential::dataRequest(byte_t* data, length_t length, length_t offset){
	// check length
	if(!length){
		return 0;
	};
	
	// check data existence, offset, length
	int ret = checkBufferLength(data, this->data_up_length, length, offset);
	if(ret < 0){
		return ret;
	};
	
	// try to acquire mutex
	#ifdef THREADING
		if(!this->busy.try_lock()){
			return 0;
		};
	#endif
	
	// access the buffer and copy data
	for(length_t i = 0; i < length; i++){
		data[i] = this->data_up[offset + i];
	};
	
	MUTEX_THIS_UNLOCK(busy)
	
	return length;
} // dataRequest(byte_t*, length_t, length_t)

int Sequential::dataRequest(length_t length, length_t offset){
	// check length
	if(!length){
		return 0;
	};
	
	// check data existence, offset, length
	int ret = checkLength(this->data_up_length, length, offset);
	if(ret < 0){
		return ret;
	};
	
	// try to acquire mutex
	#ifdef THREADING
		if(!this->busy.try_lock()){
			return 0;
		};
	#endif
	
	// pass the pointer to the beginning of requested section in main array (the pointer is defined as const in IGeneric)
	this->iface.data(this->data_up + offset, length, offset);
	
	MUTEX_THIS_UNLOCK(busy)
	
	return length;
} // dataRequest(length_t, length_t)

void Sequential::cycle(){
	// check devices pointer
	if(!this->devices){
		return;
	};
	
	MUTEX_THIS_LOCK(busy)
	
	// get downstream data
	this->iface.dataRequest(this->data_down, this->data_down_length);
	
	// process downstream
	for(devlength_t i = 0; i < this->devices_length; i++){
		// skip device if it does not have any associated downstream data
		if(!this->devices[i].data_down.length){
			continue;
		};
		
		// sends all downstream data to device (do no check for changes)
		this->devices[i].device.coreData(this->data_down + this->devices[i].data_down.offset, this->devices[i].data_down.length);
	};
	
	// process upstream
	for(devlength_t i = 0; i < this->devices_length; i++){
		// skip device if it does not have any associated upstream data
		if(!this->devices[i].data_up.length){
			continue;
		};
		
		// request all upstream data from device
		this->devices[i].device.coreRequest(this->devices[i].data_up.length);
	};
	
	// all data sent to upstream during calls to devData
	
	MUTEX_THIS_UNLOCK(busy)
} // cycle

devlength_t Sequential::findDevice(IDevice& device){
	// check devices pointer
	if(!this->devices){
		return DEVLENGTH_MAX;
	};
	
	for(devlength_t i = 0; i < this->devices_length; i++){
		if(&(this->devices[i].device) == &device){
			return i;
		};
	};
	
	return DEVLENGTH_MAX;
} // findDevice

}} // namespace ModuCab::Core
