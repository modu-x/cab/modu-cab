/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Core
 * Base of core
 */
#ifndef MODUCAB_CORE_BASE_H_
#define MODUCAB_CORE_BASE_H_


#include "core/i_core_downstream.h"
#include "core/i_device.h"
#include "utils/threading/mutex.h"
#include "utils/types.h"
#include "utils/types_core.h"


namespace ModuCab { namespace Core {

class Base: public ICoreDownstream {
	public:
		/**
		 * Constructor
		 * @param data_in - input buffer
		 * @param data_out - output buffer
		 * @param devices - array of devices; all checks, ordering, etc. must be ensured by caller
		 * @param devices_length - length of devices array
		 */
		Base(byte_t* data_in, byte_t* data_out, const device_t* devices, devlength_t devices_length);
		
		
		/**
		 * ICoreDownstream
		 */
		int devData(IDevice* device, const byte_t* data, length_t length, length_t offset) override;
		int devRequest(IDevice* device, length_t length, length_t offset) override;
		int devRequest(IDevice* device, byte_t* data, length_t length, length_t offset) override;
	
	
	protected:
		/** Input data buffer */
		byte_t * const data_in;
		
		/** Mutex to lock input buffer for thread-safe access */
		MUTEX(data_in_lock)
		
		/** Output data buffer */
		byte_t * const data_out;
		
		/** Mutex to lock output buffer for thread-safe access */
		MUTEX(data_out_lock)
		
		/** Array of all known devices */
		const device_t * const devices;
		
		/** Length of the array */
		const devlength_t devices_length;
		
		
		/**
		 * Finds device in the array and returns its index
		 * @param device - pointer to device instance to search for
		 * @return index of device in the array or DEVLENGTH_MAX if not found (as the maximum valid index is the maximum value minus one)
		 */
		devlength_t findDevice(IDevice* device);
		
		/**
		 * Checks validity of length and offset request
		 * @param available - available length of data
		 * @param length
		 * @param offset
		 * @return zero when OK, error code from ICoreDownstream (negative number) when NOK
		 */
		int checkLength(length_t available, length_t length, length_t offset);
}; // class Base

}} // namespace ModuCab::Core


#endif
