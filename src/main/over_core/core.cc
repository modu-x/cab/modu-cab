/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "over_core/core.h"

#include <stdexcept> // C++ standard exceptions

#include "utils/checking.h"


namespace ModuCab { namespace OverCore {

using ::ModuCab::Interfaces::IGeneric;
using ::ModuCab::Utils::Checking::checkLength;
using ::ModuCab::Utils::Checking::checkBufferLength;


Core::Data::Data(const length_t length, const byte_t* init):
	length(length)
{
	// check length
	if(length < 1){
		throw ::std::invalid_argument("Length should be a positive number!");
	};
	
	// allocation of mask with value-initialization of each element to 0
	this->mask = new byte_t[(length / 8) + ((length % 8) ? 1 : 0)]();
	
	// allocate and init data buffers
	if(init != nullptr){
		// allocate buffers
		this->effective = new byte_t[length];
		this->real = new byte_t[length];
		this->overriding = new byte_t[length];
		
		// copy initial values into them
		for(length_t i = 0; i < length; i++){
			this->effective[i] = init[i];
			this->real[i] = init[i];
			this->overriding[i] = init[i];
		};
	}
	else {// allocate buffers with value-initialization of each element to 0
		this->effective = new byte_t[length]();
		this->real = new byte_t[length]();
		this->overriding = new byte_t[length]();
	};
} // constructor

Core::Data::~Data(){
	// free buffers
	delete[] this->effective;
	delete[] this->real;
	delete[] this->overriding;
	delete[] this->mask;
} // destructor


Core::Core(
	IOverface& oface,
	length_t up,
	length_t down,
	const byte_t* init_up,
	const byte_t* init_down
):
	oface(oface),
	data_up(up, init_up),
	data_down(down, init_down)
{} // constructor

void Core::injectUp(IGeneric* iface){
	if(this->iface_up == nullptr){
		this->iface_up = iface;
	};
} // injectUp

void Core::injectDown(IGeneric* iface){
	if(this->iface_down == nullptr){
		this->iface_down = iface;
	};
} // injectDown


IGeneric& Core::getIfaceUp(){
	return this->if_up;
} // getIfaceUp

IGeneric& Core::getIfaceDown(){
	return this->if_down;
} // getIfaceDown


int Core::upData(const byte_t* data, length_t length, length_t offset){
	return this->processData(this->data_down, this->iface_down, &IOverface::dataDown, data, length, offset);
} // upData

int Core::upRequest(byte_t* data, length_t length, length_t offset){
	return this->processRequest(this->data_up, this->iface_down, &IOverface::requestUp, data, length, offset);
} // upRequest

int Core::upRequestCall(length_t length, length_t offset){
	return this->processRequestCall(this->data_up, this->iface_up, this->iface_down, &IOverface::requestUp, length, offset);
} // upRequestCall


int Core::downData(const byte_t* data, length_t length, length_t offset){
	return this->processData(this->data_up, this->iface_up, &IOverface::dataUp, data, length, offset);
} // downData

int Core::downRequest(byte_t* data, length_t length, length_t offset){
	return this->processRequest(this->data_down, this->iface_up, &IOverface::requestDown, data, length, offset);
} // downRequest

int Core::downRequestCall(length_t length, length_t offset){
	return this->processRequestCall(this->data_down, this->iface_down, this->iface_up, &IOverface::requestDown, length, offset);
} // downRequestCall


int Core::overUpData(const byte_t* data, const byte_t* mask, length_t length, length_t offset){
	return this->processOverData(this->data_up, this->iface_up, data, mask, length, offset);
} // overUpData

int Core::overUpRequest(byte_t* data, byte_t* mask, length_t length, length_t offset){
	return this->processOverRequest(this->data_up, data, mask, length, offset);
} // overUpRequest(byte_t*, byte_t*, length_t, length_t)

int Core::overUpRequest(byte_t* data, length_t length, length_t offset, bool refresh){
	return this->processOverRequest(this->data_up, this->iface_down, data, length, offset, refresh);
} // overUpRequest(byte_t*, length_t, length_t, bool)


int Core::overDownData(const byte_t* data, const byte_t* mask, length_t length, length_t offset){
	return this->processOverData(this->data_down, this->iface_down, data, mask, length, offset);
} // overDownData

int Core::overDownRequest(byte_t* data, byte_t* mask, length_t length, length_t offset){
	return this->processOverRequest(this->data_down, data, mask, length, offset);
} // overDownRequest(byte_t*, byte_t*, length_t, length_t)

int Core::overDownRequest(byte_t* data, length_t length, length_t offset, bool refresh){
	return this->processOverRequest(this->data_down, this->iface_up, data, length, offset, refresh);
} // overDownRequest(byte_t*, length_t, length_t, bool)


int Core::processData(
	Core::Data& data,
	IGeneric* iface,
	int (IOverface::*f_oface)(const byte_t*, length_t, length_t),
	const byte_t* buff, length_t length, length_t offset
){
	// check arguments
	int ret = checkBufferLength(buff, data.length, length, offset);
	if(ret < 0){
		return ret;
	};
	
	// passthrough flag - true if data needs to be send also to other side
	bool pass = false;
	
	// compute offseted pointers
	byte_t* data_real = data.real + offset;
	byte_t* data_effective = data.effective + offset;
	
	// acquire buffer
	data.lock.lock();
	
	// copy data between buffers
	for(length_t i = 0; i < length; i++){
		// copy data to internal buffer of "real" values
		data_real[i] = buff[i];
		
		// copy to output buffer if not overriden
		length_t mi = offset + i;
		if(!(data.mask[mi/8] & (1 << mi%8))){
			data_effective[i] = buff[i];
			
			// set passthrough flag
			pass = true;
		};
	};
	
	// send data to over-face
	(this->oface .* f_oface)(data_effective, length, offset);
	
	// send to other side if necessary
	if(pass && iface){
		iface->data(data_effective, length, offset);
	};
	
	// release buffer
	data.lock.unlock();
	
	return length;
} // processData

int Core::processRequest(
	Core::Data& data,
	IGeneric* iface,
	int (IOverface::*f_oface)(byte_t*, byte_t*, length_t, length_t),
	byte_t* buff, length_t length, length_t offset
){
	// check arguments
	int ret = checkBufferLength(buff, data.length, length, offset);
	if(ret < 0){
		return ret;
	};
	
	// passthrough flag - true if data needs to be also requested from other side
	bool pass = false;
	
	// compute offseted pointers
	byte_t* data_overriding = data.overriding + offset;
	byte_t* data_effective = data.effective + offset;
	byte_t* data_real = data.real + offset;
	
	// acquire buffer
	data.lock.lock();
	
	// request for overriden data
	(this->oface .* f_oface)(data_overriding, data.mask + offset/8, length, offset);
	
	// copy data between buffers
	for(length_t i = 0; i < length; i++){
		// refresh effective buffer - better to do according to changed bytes by over-request
		length_t mi = offset + i;
		if(data.mask[mi/8] & (1 << mi%8)){
			data_effective[i] = data_overriding[i];
		}
		else {
			if(!pass && iface){// first passthrough value => request fresh data from other side
				iface->dataRequest(data_real + i, length - i, offset + i);
			};
			
			// copy data
			data_effective[i] = data_real[i];
			
			// set passthrough flag
			pass = true;
		};
		
		// copy to final buffer
		buff[i] = data_effective[i];
	};
	
	// release buffer
	data.lock.unlock();
	
	return length;
} // processRequest

int Core::processRequestCall(
	Core::Data& data,
	IGeneric* iface,
	IGeneric* iface_other,
	int (IOverface::*f_oface)(byte_t*, byte_t*, length_t, length_t),
	length_t length, length_t offset
){
	// check arguments
	int ret = checkLength(data.length, length, offset);
	if(ret < 0){
		return ret;
	};
	
	// passthrough flag - true if the request needs to be forwarded also to the other side
	bool fwd = false;
	
	// compute offseted pointers
	byte_t* data_overriding = data.overriding + offset;
	byte_t* data_effective = data.effective + offset;
	
	// acquire buffer
	data.lock.lock();
	
	// request for overriden data
	(this->oface .* f_oface)(data_overriding, data.mask + offset/8, length, offset);
	
	// copy data between buffers
	for(length_t i = 0; i < length; i++){
		// refresh effective buffer - better to do according to changed bytes by over-request
		length_t mi = offset + i;
		if(data.mask[mi/8] & (1 << mi%8)){
			data_effective[i] = data_overriding[i];
		}
		else {// there is at least single byte which needs to be requested
			fwd = true;// forward request to other side
		};
	};
	
	// forward request or call data directly
	if(fwd && iface_other){
		// release buffer
		data.lock.unlock();
		
		// forward
		return iface_other->dataRequest(length, offset);
	}
	else {
		// call data back
		ret = (iface) ? iface->data(data_effective, length, offset) : 0;
		
		// release buffer
		data.lock.unlock();
		
		return ret;
	};
} // processRequestCall


int Core::processOverData(
	Core::Data& data,
	IGeneric* iface,
	const byte_t* buff, const byte_t* mask, length_t length, length_t offset
){
	// check arguments
	int ret = checkLength(data.length, length, offset);
	if(ret < 0){
		return ret;
	};
	
	// passthrough flag - true if data needs to be send also to other side
	bool pass = false;
	
	// compute offseted pointers
	byte_t* data_overriding = data.overriding + offset;
	byte_t* data_effective = data.effective + offset;
	byte_t* data_real = data.real + offset;
	
	// acquire buffer
	data.lock.lock();
	
	// copy data between buffers
	if(buff && mask){// update both
		// set passthrough flag directly - otherwise we have to check for change of every single byte
		pass = true;
		
		for(length_t i = 0; i < length; i++){
			// copy data to internal buffer of overriding values
			data_overriding[i] = buff[i];
			
			// copy data to internal buffer of over-mask
			length_t mi = offset + i;
			length_t mi2 = offset%8 + i;
			data.mask[mi/8] = (data.mask[mi/8] & ~(1 << mi%8)) | (mask[mi2/8] & ~(1 << mi2%8));//TODO
			
			// refresh output buffer
			if(data.mask[mi/8] & (1 << mi%8)){
				data_effective[i] = data_overriding[i];
			}
			else {
				data_effective[i] = data_real[i];
			};
		};
	}
	else if(mask){// update over-mask only
		// set passthrough flag directly - otherwise we have to check for change of every single byte
		pass = true;
		
		for(length_t i = 0; i < length; i++){
			// copy data to internal buffer of over-mask
			length_t mi = offset + i;
			length_t mi2 = offset%8 + i;
			data.mask[mi/8] = (data.mask[mi/8] & ~(1 << mi%8)) | (mask[mi2/8] & ~(1 << mi2%8));//TODO
			
			// refresh output buffer
			if(data.mask[mi/8] & (1 << mi%8)){
				data_effective[i] = data_overriding[i];
			}
			else {
				data_effective[i] = data_real[i];
			};
		};
	}
	else if(buff){// update over-data only
		for(length_t i = 0; i < length; i++){
			// copy data to internal buffer of overriding values
			data_overriding[i] = buff[i];
			
			// copy to output buffer if overriden
			length_t mi = offset + i;
			if(data.mask[mi/8] & (1 << mi%8)){
				data_effective[i] = buff[i];
				
				// set passthrough flag
				pass = true;
			};
		};
	}
	else {// only null-pointers given, nothing to do
		// release buffer
		data.lock.unlock();
		
		return -1;
	};
	
	// send to other side if necessary
	if(pass && iface){
		iface->data(data_effective, length, offset);
	};
	
	// release buffer
	data.lock.unlock();
	
	return length;
} // processOverData

int Core::processOverRequest(
	Core::Data& data,
	byte_t* buff, byte_t* mask, length_t length, length_t offset
){
	// check arguments
	int ret = checkLength(data.length, length, offset);
	if(ret < 0){
		return ret;
	};
	
	// compute offseted pointers
	byte_t* data_overriding = data.overriding + offset;
	byte_t* data_mask = data.mask + offset/8;
	
	// acquire buffer
	data.lock.lock();
	
	// copy over-data
	if(buff){
		for(length_t i = 0; i < length; i++){
			buff[i] = data_overriding[i];
		};
	};
	
	// copy over-mask
	if(mask){
		// process first incomplete byte
		unsigned char mr = offset % 8;// bits to skip from beginning of byte
		if(mr){
			unsigned char mm = 0xFF;
			if(length < (unsigned char)(8 - mr)){// length shorter than rest of the byte
				mm = ~(mm << length);
			};
			
			mm <<= mr;
			
			// copy bits
			mask[0] = (mask[0] & ~mm) | (data_mask[0] & mm);
		};
		
		// process complete bytes
		length_t ml = length / 8;
		for(length_t i = ((mr) ? 1 : 0); i < ml; i++){
			mask[i] = data_mask[i];
		};
		
		// process last incomplete byte
		mr = length % 8;
		if(mr){
			unsigned char mm = (0xFF << mr);// mm is negative, operators swapped on next line
			mask[ml] = (mask[ml] & mm) | (data_mask[ml] & ~mm);
		};
	};
	
	// release buffer
	data.lock.unlock();
	
	return (buff || mask) ? length : -1;
} // processOverRequest(Core::Data&, byte_t*, byte_t*, length_t, length_t)

int Core::processOverRequest(
	Core::Data& data,
	IGeneric* iface,
	byte_t* buff, length_t length, length_t offset, bool refresh
){
	// check arguments
	int ret = checkBufferLength(buff, data.length, length, offset);
	if(ret < 0){
		return ret;
	};
	
	// compute offseted pointers
	byte_t* data_real = data.real + offset;
	
	// acquire buffer
	data.lock.lock();
	
	// request fresh data from other side
	if(refresh && iface){
		iface->dataRequest(data_real, length, offset);
	};
	
	// copy data between buffers
	for(length_t i = 0; i < length; i++){
		buff[i] = data_real[i];
	};
	
	// release buffer
	data.lock.unlock();
	
	return length;
} // processOverRequest(Core::Data&, IGeneric*, byte_t*, length_t, length_t, bool)

}} // namespace ModuCab::OverCore
