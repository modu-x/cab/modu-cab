/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::OverCore
 * Core interface available to over-face
 */
#ifndef MODUCAB_OVERCORE_ICORE_H_
#define MODUCAB_OVERCORE_ICORE_H_


#include "utils/types.h"


namespace ModuCab { namespace OverCore {

class ICore {
	public:
		/**
		 * Overriding up-stream data update
		 * @param data - data array
		 * @param mask - overriding mask array (for simple handling, it will be aligned down to whole byte - e.g. for offset=3, the mask will begin at offset=0)
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @return number of bytes received or negative as error code
		 * 
		 * Accepts null-pointer in data or mask to signalize update of one of them only.
		 */
		virtual int overUpData(const byte_t* data, const byte_t* mask, length_t length, length_t offset = 0) =0;
		
		/**
		 * Request for up-stream overriding data
		 * @param data - pointer to data array
		 * @param mask - overriding mask array (for simple handling, it will be aligned down to whole byte - e.g. for offset=3, the mask will begin at offset=0)
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @return number of bytes passed or negative as error code
		 * 
		 * Accepts null-pointer in data or mask to signalize update of one of them only.
		 */
		virtual int overUpRequest(byte_t* data, byte_t* mask, length_t length, length_t offset = 0) =0;
		
		/**
		 * Request for real up-stream data
		 * @param data - pointer to data array
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @param refresh - whether to request actual data from up-stream interface
		 * @return number of bytes passed or negative as error code
		 */
		virtual int overUpRequest(byte_t* data, length_t length, length_t offset = 0, bool refresh = false) =0;
		
		/**
		 * Overriding down-stream data update
		 * @param data - data array
		 * @param mask - overriding mask array (for simple handling, it will be aligned down to whole byte - e.g. for offset=3, the mask will begin at offset=0)
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @return number of bytes received or negative as error code
		 * 
		 * Accepts null-pointer in data or mask to signalize update of one of them only.
		 */
		virtual int overDownData(const byte_t* data, const byte_t* mask, length_t length, length_t offset = 0) =0;
		
		/**
		 * Request for down-stream overriding data
		 * @param data - pointer to data array
		 * @param mask - overriding mask array (for simple handling, it will be aligned down to whole byte - e.g. for offset=3, the mask will begin at offset=0)
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @return number of bytes passed or negative as error code
		 * 
		 * Accepts null-pointer in data or mask to signalize update of one of them only.
		 */
		virtual int overDownRequest(byte_t* data, byte_t* mask, length_t length, length_t offset = 0) =0;
		
		/**
		 * Request for down-stream real data
		 * @param data - pointer to data array
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @param refresh - whether to request actual data from down-stream interface
		 * @return number of bytes passed or negative as error code
		 */
		virtual int overDownRequest(byte_t* data, length_t length, length_t offset = 0, bool refresh = false) =0;
}; // class ICore

}} // namespace ModuCab::OverCore


#endif
