/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::OverCore
 * OverCore interface
 */
#ifndef MODUCAB_OVERCORE_IOVERFACE_H_
#define MODUCAB_OVERCORE_IOVERFACE_H_


#include "utils/types.h"


namespace ModuCab { namespace OverCore {

class IOverface {
	public:
		/**
		 * Real up-stream data update
		 * @param data - data array
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @return number of bytes received or negative as error code
		 */
		virtual int dataUp(const byte_t* data, length_t length, length_t offset = 0) =0;
		
		/**
		 * Request for up-stream overriding data
		 * @param data - pointer to data array
		 * @param mask - overriding mask array (for simple handling, it will be aligned down to whole byte - e.g. for offset=3, the mask will begin at offset=0)
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @return number of bytes passed or negative as error code
		 * 
		 * Accepts null-pointer in data or mask to signalize update of one of them only.
		 */
		virtual int requestUp(byte_t* data, byte_t* mask, length_t length, length_t offset = 0) =0;
		
		/**
		 * Real down-stream data update
		 * @param data - data array
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @return number of bytes received or negative as error code
		 */
		virtual int dataDown(const byte_t* data, length_t length, length_t offset = 0) =0;
		
		/**
		 * Request for down-stream overriding data
		 * @param data - pointer to data array
		 * @param mask - overriding mask array (for simple handling, it will be aligned down to whole byte - e.g. for offset=3, the mask will begin at offset=0)
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @return number of bytes passed or negative as error code
		 * 
		 * Accepts null-pointer in data or mask to signalize update of one of them only.
		 */
		virtual int requestDown(byte_t* data, byte_t* mask, length_t length, length_t offset = 0) =0;
}; // class IOverface

}} // namespace ModuCab::OverCore


#endif
