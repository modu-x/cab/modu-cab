/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::OverCore::Interfaces
 * Simple debugging interface that dumps anything to given output
 */
#ifndef MODUCAB_OVERCORE_INTERFACES_DUMPER_H_
#define MODUCAB_OVERCORE_INTERFACES_DUMPER_H_


#include <cstdio> // FILE

#include "over_core/i_overface.h"


namespace ModuCab { namespace OverCore { namespace Interfaces {

class Dumper: public ::ModuCab::OverCore::IOverface {
	public:
		/**
		 * Constructor
		 * @param stream - debugging output
		 */
		Dumper(FILE* stream = stderr);
		
		/**
		 * IOverface
		 */
		int dataUp(const byte_t* data, length_t length, length_t offset) override;
		int requestUp(byte_t* data, byte_t* mask, length_t length, length_t offset) override;
		int dataDown(const byte_t* data, length_t length, length_t offset) override;
		int requestDown(byte_t* data, byte_t* mask, length_t length, length_t offset) override;
	
	
	private:
		/** Debugging output */
		FILE* stream;
}; // class Dumper

}}} // namespace ModuCab::OverCore::Interfaces


#endif
