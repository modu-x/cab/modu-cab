/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::OverCore
 * Overriding up/down data between Core and Upstream interface
 */
#ifndef MODUCAB_OVERCORE_CORE_H_
#define MODUCAB_OVERCORE_CORE_H_


#include <mutex>

#include "interfaces/i_generic.h"
#include "over_core/i_core.h"
#include "over_core/i_overface.h"
#include "utils/interface_caller.h"
#include "utils/types.h"


namespace ModuCab { namespace OverCore {

class Core: public ICore {
	public:
		/**
		 * Constructor
		 * @param oface - reference to over-core interface
		 * @param up - length of up-stream data
		 * @param down - length of down-stream data
		 * @param init_up - buffer with initial values for up-stream data or null-pointer to initialize each element with 0
		 * @param init_down - buffer with initial values for down-stream data or null-pointer to initialize each element with 0
		 */
		Core(
			IOverface& oface,
			length_t up,
			length_t down,
			const byte_t* init_up = nullptr,
			const byte_t* init_down = nullptr
		);
		
		/**
		 * Injects up-stream interface
		 * @param iface - reference to interface
		 */
		void injectUp(::ModuCab::Interfaces::IGeneric* iface);
		
		/**
		 * Injects down-stream interface
		 * @param iface - reference to interface
		 */
		void injectDown(::ModuCab::Interfaces::IGeneric* iface);
		
		
		/**
		 * Returns up-stream side interface
		 * @return interface
		 */
		::ModuCab::Interfaces::IGeneric& getIfaceUp();
		
		/**
		 * Returns down-stream side interface
		 * @return interface
		 */
		::ModuCab::Interfaces::IGeneric& getIfaceDown();
		
		
		/**
		 * Translated IGeneric methods for up-stream
		 */
		int upData(const byte_t* data, length_t length, length_t offset = 0);
		int upRequest(byte_t* data, length_t length, length_t offset = 0);
		int upRequestCall(length_t length, length_t offset = 0);
		
		/**
		 * Translated IGeneric methods for down-stream
		 */
		int downData(const byte_t* data, length_t length, length_t offset = 0);
		int downRequest(byte_t* data, length_t length, length_t offset = 0);
		int downRequestCall(length_t length, length_t offset = 0);
		
		
		/**
		 * ICore
		 */
		int overUpData(const byte_t* data, const byte_t* mask, length_t length, length_t offset) override;
		int overUpRequest(byte_t* data, byte_t* mask, length_t length, length_t offset) override;
		int overUpRequest(byte_t* data, length_t length, length_t offset, bool refresh) override;
		int overDownData(const byte_t* data, const byte_t* mask, length_t length, length_t offset) override;
		int overDownRequest(byte_t* data, byte_t* mask, length_t length, length_t offset) override;
		int overDownRequest(byte_t* data, length_t length, length_t offset, bool refresh) override;
	
	
	private:
		/**
		 * Data buffers for one stream
		 */
		class Data {
			public:
				/**
				 * Constructor
				 * @param length - length of data buffers
				 * @param init - buffer to copy initial values from or null-pointer to initialize each element with 0
				 */
				Data(const length_t length, const byte_t* init = nullptr);
				
				/**
				 * Destructor
				 */
				~Data();
				
				
				/** Data buffers */
				const length_t length;
				byte_t* effective;
				byte_t* real;
				byte_t* overriding;
				
				/** Data overriding bit-mask - if bit set, the corresponding byte in data buffer will be overridden */
				byte_t* mask;
				
				/** Mutex to lock data buffers for thread-safe access */
				::std::mutex lock;
		};
		
		
		/** Up-stream IGeneric interface caller */
		::ModuCab::Utils::InterfaceCaller<Core> if_up = {
			*this,
			&Core::upData,
			&Core::upRequest,
			&Core::upRequestCall
		};
		
		/** Down-stream IGeneric interface caller */
		::ModuCab::Utils::InterfaceCaller<Core> if_down = {
			*this,
			&Core::downData,
			&Core::downRequest,
			&Core::downRequestCall
		};
		
		/** Reference to up-stream interface */
		::ModuCab::Interfaces::IGeneric* iface_up;
		
		/** Reference to down-stream interface */
		::ModuCab::Interfaces::IGeneric* iface_down;
		
		
		/** Reference to over-core interface */
		IOverface& oface;
		
		
		/** Up-stream data */
		Data data_up;
		
		/** Down-stream data */
		Data data_down;
		
		
		/**
		 * Copies data between buffers
		 * @param data - buffers to operate on
		 * @param iface - interface for data passthrough
		 * @param f_oface - pointer to over-face real data passing method
		 * @param buff - pointer to byte array
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @return number of bytes copied or negative as error code
		 */
		int processData(
			Data& data,
			::ModuCab::Interfaces::IGeneric* iface,
			int (IOverface::*f_oface)(const byte_t*, length_t, length_t),
			const byte_t* buff, length_t length, length_t offset
		);
		
		/**
		 * Copies data between buffers
		 * @param data - buffers to operate on
		 * @param iface - interface for request passthrough
		 * @param f_oface - pointer to over-face overriding data request method
		 * @param buff - pointer to byte array
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @return number of bytes copied or negative as error code
		 */
		int processRequest(
			Data& data,
			::ModuCab::Interfaces::IGeneric* iface,
			int (IOverface::*f_oface)(byte_t*, byte_t*, length_t, length_t),
			byte_t* buff, length_t length, length_t offset
		);
		
		/**
		 * Forwards request
		 * @param data - buffers to operate on
		 * @param iface - interface to call back
		 * @param iface_other - interface for request passthrough
		 * @param f_oface - pointer to over-face overriding data request method
		 * @param buff - pointer to byte array
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @return number of bytes copied or negative as error code
		 */
		int processRequestCall(
			Data& data,
			::ModuCab::Interfaces::IGeneric* iface,
			::ModuCab::Interfaces::IGeneric* iface_other,
			int (IOverface::*f_oface)(byte_t*, byte_t*, length_t, length_t),
			length_t length, length_t offset
		);
		
		/**
		 * Copies data between buffers
		 * @param data - buffers to operate on
		 * @param iface - interface for data passthrough
		 * @param buff - pointer to over-data array
		 * @param mask - pointer to over-mask array
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @return number of bytes copied or negative as error code
		 */
		int processOverData(
			Data& data,
			::ModuCab::Interfaces::IGeneric* iface,
			const byte_t* buff, const byte_t* mask, length_t length, length_t offset
		);
		
		/**
		 * Processes request for overriding data
		 * @param data - buffers to operate on
		 * @param buff - pointer to over-data array
		 * @param mask - overriding mask array
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @return number of bytes passed or negative as error code
		 */
		int processOverRequest(
			Data& data,
			byte_t* buff, byte_t* mask, length_t length, length_t offset
		);
		
		/**
		 * Processes request for real data
		 * @param data - buffers to operate on
		 * @param buff - pointer to over-data array
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @param refresh - whether to request actual data from other side
		 * @return number of bytes passed or negative as error code
		 */
		int processOverRequest(
			Data& data,
			::ModuCab::Interfaces::IGeneric* iface,
			byte_t* buff, length_t length, length_t offset, bool refresh = false
		);
}; // class Core

}} // namespace ModuCab::OverCore


#endif
