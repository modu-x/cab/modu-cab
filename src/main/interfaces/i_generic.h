/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Interfaces
 * Generic interface for data passing
 */
#ifndef MODUCAB_INTERFACES_IGENERIC_H_
#define MODUCAB_INTERFACES_IGENERIC_H_


#include "utils/types.h"


namespace ModuCab { namespace Interfaces {

class IGeneric {
	public:
		/**
		 * Data from calling code
		 * @param data - pointer to byte array
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @return number of bytes received or negative as error code
		 */
		virtual int data(const byte_t* data, length_t length, length_t offset = 0) =0;
		
		/**
		 * Request for data for calling code
		 * @param data - pointer to byte array, where the calling code wants to store the requested data
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @return number of bytes passed or negative as error code
		 *
		 * The called code will put the requested data into the pointed array.
		 * If multi-threaded, the calling code must take care about possible locking/unlocking the data-mutex before/after call to this method.
		 */
		virtual int dataRequest(byte_t* data, length_t length, length_t offset = 0) =0;
		
		/**
		 * Request for data for calling code
		 * @param length - number of bytes
		 * @param offset - byte offset of data in whole array
		 * @return number of bytes passed or negative as error code
		 *
		 * The called code responds to this request by subsequent call to calling code's data(...) method.
		 * If multi-threaded, the method will be called from the same thread, so be aware of not creating deadlock situation.
		 */
		virtual int dataRequest(length_t length, length_t offset = 0) =0;
}; // class IGeneric

}} // namespace ModuCab::Interfaces


#endif
