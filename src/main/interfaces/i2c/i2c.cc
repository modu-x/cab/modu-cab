/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "interfaces/i2c/i2c.h"


namespace ModuCab { namespace Interfaces { namespace I2C {

I2C::I2C(IDriver& driver, bool broadcast):
	driver(driver),
	broadcast(broadcast)
{}// constructor

int I2C::read(byte_t addr, byte_t* data, unsigned char length){
	addr = addr & 0x7F;
	
	if(addr == 0){
		return IDriver::BAD_ADDRESS_OR_BROADCAST_NOT_ALLOWED;
	};
	
	return this->driver.read(addr, data, length);
} // read

int I2C::write(byte_t addr, const byte_t* data, unsigned char length){
	addr = addr & 0x7F;
	
	if(addr == 0 && !this->broadcast){
		return IDriver::BAD_ADDRESS_OR_BROADCAST_NOT_ALLOWED;
	};
	
	return this->driver.write(addr, data, length);
} // write

int I2C::bytes(byte_t addr, const byte_t* wdata, unsigned char wlength, byte_t* rdata, unsigned char rlength){
	addr = addr & 0x7F;
	
	if(addr == 0){
		return IDriver::BAD_ADDRESS_OR_BROADCAST_NOT_ALLOWED;
	};
	
	return this->driver.bytes(addr, wdata, wlength, rdata, rlength);
} // bytes

}}} // namespace ModuCab::Interfaces::I2C
