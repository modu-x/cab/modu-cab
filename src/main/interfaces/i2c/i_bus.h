/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Interfaces::I2C
 * I2C Bus interface
 */
#ifndef MODUCAB_INTERFACES_I2C_IBUS_H_
#define MODUCAB_INTERFACES_I2C_IBUS_H_


#include "interfaces/i2c/i_driver.h"
#include "utils/types.h"


namespace ModuCab { namespace Interfaces { namespace I2C {

class IBus: public IDriver {
	public:
		/**
		 * Writes single byte to I2C slave
		 * @param addr - slave address (7-bit)
		 * @param data - byte to write
		 * @return positive = number of bytes written, negative = error code
		 */
		virtual int write_8(byte_t addr, const byte_t data) =0;
		
		/**
		 * Writes 2 bytes to I2C slave
		 * @param addr - slave address (7-bit)
		 * @param data0 - first byte to write
		 * @param data1 - second byte to write
		 * @return positive = number of bytes written, negative = error code
		 */
		virtual int write_16(byte_t addr, const byte_t data0, const byte_t data1) =0;
		
		/**
		 * Writes 3 bytes to I2C slave
		 * @param addr - slave address (7-bit)
		 * @param data0 - first byte to write
		 * @param data1 - second byte to write
		 * @param data2 - third byte to write
		 * @return positive = number of bytes written, negative = error code
		 */
		virtual int write_24(byte_t addr, const byte_t data0, const byte_t data1, const byte_t data2) =0;
		
		/**
		 * Writes 4 bytes to I2C slave
		 * @param addr - slave address (7-bit)
		 * @param data0 - first byte to write
		 * @param data1 - second byte to write
		 * @param data2 - third byte to write
		 * @param data3 - fourth byte to write
		 * @return positive = number of bytes written, negative = error code
		 */
		virtual int write_32(byte_t addr, const byte_t data0, const byte_t data1, const byte_t data2, const byte_t data3) =0;
		
		/**
		 * Transaction with I2C slave consisting of single byte write followed by read separated by repeated start bit (no stop bit between)
		 * @param addr - slave address (7-bit)
		 * @param wdata - byte to write
		 * @param rdata - pointer to array of bytes to store the read data
		 * @param rlength - number of bytes to read
		 * @return positive = number of read bytes, negative = error code
		 */
		virtual int bytes_8(byte_t addr, const byte_t wdata, byte_t* rdata, unsigned char rlength) =0;
		
		/**
		 * Transaction with I2C slave consisting of 2 bytes write followed by read separated by repeated start bit (no stop bit between)
		 * @param addr - slave address (7-bit)
		 * @param wdata0 - first byte to write
		 * @param wdata1 - second byte to write
		 * @param rdata - pointer to array of bytes to store the read data
		 * @param rlength - number of bytes to read
		 * @return positive = number of read bytes, negative = error code
		 */
		virtual int bytes_16(byte_t addr, const byte_t wdata0, const byte_t wdata1, byte_t* rdata, unsigned char rlength) =0;
		
		/**
		 * Transaction with I2C slave consisting of 3 bytes write followed by read separated by repeated start bit (no stop bit between)
		 * @param addr - slave address (7-bit)
		 * @param wdata0 - first byte to write
		 * @param wdata1 - second byte to write
		 * @param wdata2 - third byte to write
		 * @param rdata - pointer to array of bytes to store the read data
		 * @param rlength - number of bytes to read
		 * @return positive = number of read bytes, negative = error code
		 */
		virtual int bytes_24(byte_t addr, const byte_t wdata0, const byte_t wdata1, const byte_t wdata2, byte_t* rdata, unsigned char rlength) =0;
		
		/**
		 * Transaction with I2C slave consisting of 4 bytes write followed by read separated by repeated start bit (no stop bit between)
		 * @param addr - slave address (7-bit)
		 * @param wdata0 - first byte to write
		 * @param wdata1 - second byte to write
		 * @param wdata2 - third byte to write
		 * @param wdata3 - fourth byte to write
		 * @param rdata - pointer to array of bytes to store the read data
		 * @param rlength - number of bytes to read
		 * @return positive = number of read bytes, negative = error code
		 */
		virtual int bytes_32(byte_t addr, const byte_t wdata0, const byte_t wdata1, const byte_t wdata2, const byte_t wdata3, byte_t* rdata, unsigned char rlength) =0;
}; // class IBus

}}} // namespace ModuCab::Interfaces::I2C


#endif
