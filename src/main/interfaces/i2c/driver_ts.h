/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Interfaces::I2C
 * I2C Thread-Safe Driver container
 */
#ifndef MODUCAB_INTERFACES_I2C_DRIVERTS_H_
#define MODUCAB_INTERFACES_I2C_DRIVERTS_H_


#include "interfaces/i2c/i_driver.h"
#include "utils/threading/mutex.h"


namespace ModuCab { namespace Interfaces { namespace I2C {

class DriverTS: public IDriver {
	public:
		/**
		 * Constructor
		 * @param driver - I2C driver to wrap
		 */
		DriverTS(IDriver& driver);
		
		
		/**
		 * IDriver
		 */
		int read(byte_t addr, byte_t* data, unsigned char length) override;
		int write(byte_t addr, const byte_t* data, unsigned char length) override;
		int bytes(byte_t addr, const byte_t* wdata, unsigned char wlength, byte_t* rdata, unsigned char rlength) override;
	
	
	private:
		/** Reference to underlying driver */
		IDriver& driver;
		
		/** Mutex to lock bus for thread-safe access */
		MUTEX(bus)
}; // class DriverTS

}}} // namespace ModuCab::Interfaces::I2C


#endif
