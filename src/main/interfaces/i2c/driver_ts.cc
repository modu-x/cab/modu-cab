/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "interfaces/i2c/driver_ts.h"


namespace ModuCab { namespace Interfaces { namespace I2C {

DriverTS::DriverTS(IDriver& driver):
	driver(driver)
{}// constructor

#define TS_WRAPPER(F_NAME, F_PARAMS, F_CALL)\
int DriverTS::F_NAME(F_PARAMS){\
	/* acquire bus */\
	MUTEX_THIS_LOCK(bus)\
	\
	/* call wrapped driver */\
	int ret;\
	try {\
		ret = this->driver.F_NAME(F_CALL);\
	}\
	catch(...){/* catch any exception */\
		/* release bus */\
		MUTEX_THIS_UNLOCK(bus)\
		\
		/* re-throw it */\
		throw;\
	};\
	\
	/* release bus */\
	MUTEX_THIS_UNLOCK(bus)\
	\
	/* return result */\
	return ret;\
} // F_NAME

#define COMMA ,
TS_WRAPPER(read, byte_t addr COMMA byte_t* data COMMA unsigned char length, addr COMMA data COMMA length)
TS_WRAPPER(write, byte_t addr COMMA const byte_t* data COMMA unsigned char length, addr COMMA data COMMA length)
TS_WRAPPER(bytes, byte_t addr COMMA const byte_t* wdata COMMA unsigned char wlength COMMA byte_t* rdata COMMA unsigned char rlength, addr COMMA wdata COMMA wlength COMMA rdata COMMA rlength)

}}} // namespace ModuCab::Interfaces::I2C
