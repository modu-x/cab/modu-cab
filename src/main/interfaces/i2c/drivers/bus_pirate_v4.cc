/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "interfaces/i2c/drivers/bus_pirate_v4.h"

#include <unistd.h> // sleep

#include <cstdio> // snprintf, sscanf
#include <cstring> // strncmp, strlen
#include <stdexcept> // C++ standard exceptions

#include "utils/types.h"


// Replies
#define REPLY_I2C_MODE "I2C mode:\r\n 1. Software\r\n 2. Hardware\r\n"
#define REPLY_I2C_SPEEDS_SW "Set speed:\r\n 1. ~5KHz\r\n 2. ~50KHz\r\n 3. ~100KHz\r\n 4. ~400KHz\r\n"
#define REPLY_I2C_SPEEDS_HW "Set speed:\r\n 1. 100KHz\r\n 2. 400KHz\r\n 3. 1MHz\r\n"
#define REPLY_PULLUPS_SOURCE "Select Vpu (Pullup) Source:\r\n 1) External (or None)\r\n 2) Onboard 3.3v\r\n 3) Onboard 5.0v\r\n"
#define REPLY_PULLUPS_SOURCE_WARNING "Warning: already a voltage on Vpullup pin\r\n"
#define REPLY_PULLUPS_ON "Pull-up resistors ON\r\n"
#define REPLY_PULLUPS_OFF "Pull-up resistors OFF\r\n"
#define REPLY_PSU_ON "POWER SUPPLIES ON\r\n"
#define REPLY_PSU_OFF "POWER SUPPLIES OFF\r\n"


namespace ModuCab { namespace Interfaces { namespace I2C { namespace Drivers {

BusPirateV4::BusPirateV4(::ModuCab::Interfaces::Serial::ISerial& serial, ModeI2C mode, PullUps pull_ups, bool power, bool force):
	serial(serial)
{
	// check serial baudrate
	if(this->serial.getBaudrate() != 115200){
		//TODO log("The default baudrate for BusPirate is 115200, but the current serial baudrate is different. Please check, that you also switched the BusPirate to the same baudrate.");
	};
	
	// flush old data from OS buffer
	this->serial.writec("\n\n\n", 3);
	this->serialPurge();
	
	// try to get command line
	if(this->serialWrite("\r\n", 2) != 2){
		throw ::std::runtime_error("Unable to write the needed data to given serial!");
	};
	
	// check, if BusPirate is in expected "HiZ" mode
	if(!this->checkMode("HiZ")){
		if(force){
			//TODO log("Forcing BusPirate mode switching to HiZ.");
			
			// flush rest of returned data from OS buffer
			this->serialPurge();
			
			// try to switch it and check it again
			if(!this->switchModeHiZ()){
				throw ::std::runtime_error("Failed to force the BusPirate to HiZ mode!");
			};
		}
		else {
			throw ::std::runtime_error("BusPirate is not in HiZ mode! Switch it manualy before starting ModuCab or try to force its switching.");
		};
	};
	
	// check BusPirate SW version
	if(!this->checkVersion() && !this->checkVersion()){// double check needed as BusPirate mostly returns also the prompt together with command echo (it should not), but sometimes not (correct behaviour, I think)
		throw ::std::runtime_error("Unsupported BusPirate revision! Cannot handle it as there might differencies.");
	};
	
	// switch to I2C mode
	if(!this->switchModeI2C(mode)){
		throw ::std::runtime_error("Failed to switch the BusPirate to I2C mode!");
	};
	
	// pull-ups
	if(!this->setPullUps(pull_ups)){
		this->switchModeHiZ();
		throw ::std::runtime_error("Failed to set pull-ups mode!");
	};
	
	// power supplies
	if(power || static_cast<unsigned char>(pull_ups) > 1){
		if(!this->setPSUs(true)){
			this->switchModeHiZ();
			throw ::std::runtime_error("Failed to turn on power supplies!");
		};
	};
} // constructor

BusPirateV4::~BusPirateV4(){
	// switch BusPirate to safe HiZ mode
	this->switchModeHiZ();
} // destructor

int BusPirateV4::read(byte_t addr, byte_t* data, unsigned char length){
	// check arguments
	if(!data){
		return -1;
	}
	else if(length < 1){
		return 0;
	};
	
	// send commands
	if(!this->dataFormatStart(addr, true)){return -10;};
	if(!this->dataFormatRead(length)){return -12;};
	if(!this->dataFormatStop()){return -13;};
	
	// wait for bus
	//TODO
	
	// parse responses
	if(!this->dataParseStart(addr, true)){return -20;};
	unsigned char ret = this->dataParseRead(data, length);
	if(ret < length){return ret;};
	if(!this->dataParseStop()){return -23;};
	
	// check prompt
	if(!this->checkMode("I2C")){return -1;};
	
	return ret;
} // read

int BusPirateV4::write(byte_t addr, const byte_t* data, unsigned char length){
	// check arguments
	if(!data){
		return -1;
	}
	else if(length < 1){
		return 0;
	};
	
	// send commands
	if(!this->dataFormatStart(addr, false)){return -10;};
	if(this->dataFormatWrite(data, length) != length){return -11;};
	if(!this->dataFormatStop()){return -13;};
	
	// parse responses
	if(!this->dataParseStart(addr, false)){return -20;};
	unsigned char ret = this->dataParseWrite(data, length);
	if(ret != length){return ret;};
	if(!this->dataParseStop()){return -23;};
	
	// check prompt
	if(!this->checkMode("I2C")){return -1;};
	
	return ret;
} // write

int BusPirateV4::bytes(byte_t addr, const byte_t* wdata, unsigned char wlength, byte_t* rdata, unsigned char rlength){
	// check arguments
	if(!wdata || !rdata){
		return -1;
	}
	else if(wlength < 1 || rlength < 1){
		return 0;
	};
	
	// send commands
	if(!this->dataFormatStart(addr, false)){return -10;};
	if(this->dataFormatWrite(wdata, wlength) != wlength){return -11;};
	if(!this->dataFormatStart(addr, true)){return -10;};
	if(!this->dataFormatRead(rlength)){return -12;};
	if(!this->dataFormatStop()){return -13;};
	
	// wait for bus
	//TODO
	
	// parse responses
	if(!this->dataParseStart(addr, false)){return -20;};
	if(this->dataParseWrite(wdata, wlength) != wlength){return -21;};
	if(!this->dataParseStart(addr, true)){return -20;};
	unsigned char ret = this->dataParseRead(rdata, rlength);
	if(ret != rlength){return ret;};
	if(!this->dataParseStop()){return -23;};
	
	// check prompt
	if(!this->checkMode("I2C")){return -1;};
	
	return ret;
} // bytes

bool BusPirateV4::dataFormatStart(byte_t addr, bool read){
	char buffer[6];
	
	if(::snprintf(buffer, 6, "[%3u ", ((addr << 1) | (byte_t) read)) < 5){
		return false;
	};
	
	return (this->serialWrite(buffer, 5) >= 0);
} // dataFormatStart

bool BusPirateV4::dataFormatStop(){
	return (this->serialWrite("]\n", 2, 1) >= 0);
} // dataFormatStop

bool BusPirateV4::dataFormatRead(unsigned char length){
	char buffer[6];
	
	if(::snprintf(buffer, 6, "r:%u  ", length) < 5){
		return false;
	};
	
	return (this->serialWrite(buffer, 5) >= 0);
} // dataFormatRead

unsigned char BusPirateV4::dataFormatWrite(const byte_t* data, unsigned char length){
	char buffer[5];
	
	for(unsigned char i = 0; i < length; i++){
		if(::snprintf(buffer, 5, "%u   ", data[i]) < 4){
			return i;
		};
		
		if(this->serialWrite(buffer, 4) < 0){
			return i;
		};
	};
	
	return length;
} // dataFormatWrite

bool BusPirateV4::dataParseStart(byte_t addr, bool read){
	char buffer[15];
	return (this->serialReadReply(buffer, 15, "I2C START BIT\r\n") >= 0 && this->dataParseWriteByte((addr << 1) | (byte_t) read));
} // dataParseStart

bool BusPirateV4::dataParseStop(){
	char buffer[14];
	return (this->serialReadReply(buffer, 14, "I2C STOP BIT\r\n") >= 0);
} // dataParseStop

unsigned char BusPirateV4::dataParseRead(byte_t* data, unsigned char length){
	if(length < 1){
		return 0;
	};
	
	char buffer[8];
	
	// check beginning
	if(this->serialReadReply(buffer, 6, "READ: ") < 0){
		return 0;
	};
	
	// check data
	for(unsigned char i = 0; i < length; i++){
		if((i > 0 && this->serialReadReply(buffer, 5, " ACK ") < 0) || !this->dataParseReadByte(data + i)){
			return i;
		};
	};
	
	// check ending
	this->serialReadReply(buffer, 8, "\r\nNACK\r\n");//TODO no practical way to signalize as all requested data was read
	
	return length;
} // dataParseRead

bool BusPirateV4::dataParseReadByte(byte_t* data){
	char buffer[5];
	
	// read reply
	if(this->serial.readNc(buffer, 5, 20, true) != 5){
		return false;
	};
	
	// parse it
	unsigned int scandata;
	if(::sscanf(buffer, "0x%02X ", &scandata) != 1){
		return false;
	};
	
	// save
	*data = scandata;
	
	return true;
} // dataParseReadByte

unsigned char BusPirateV4::dataParseWrite(const byte_t* data, unsigned char length){
	for(unsigned char i = 0; i < length; i++){
		if(!this->dataParseWriteByte(data[i])){
			return i;
		};
	};
	
	return length;
} // dataParseWrite

bool BusPirateV4::dataParseWriteByte(const byte_t data){
	char buffer[18];
	
	// read reply
	if(this->serial.readNc(buffer, 18, 20, true) != 18){
		return false;
	};
	
	// check it
	unsigned int replied;
	if(::sscanf(buffer, "WRITE: 0x%02X ", &replied) != 1 || replied > 255 || replied != data){
		return false;
	};
	
	// check (N)ACK & purge rest according to it (for NACK, one more character needs to be read)
	return (
		!::strncmp(buffer+11, " ACK \r\n", 7)
		|| (!::strncmp(buffer+11, " NACK \r", 7) && this->serial.readNc(buffer, 1, 20, true) == 1 && buffer[0] == '\n')
	);// strncmp returns 0 when equal
} // dataParseWriteByte

bool BusPirateV4::setPullUps(PullUps mode){
	char buffer[150];
	
	if(mode == PullUps::None){// disable
		if(this->serialWrite("p\n", 2, 1) != 2){
			return false;
		};
		
		if(this->serialReadReply(buffer, 100, REPLY_PULLUPS_OFF, true) < 0){
			return false;
		};
	}
	else {
		// select source
		if(this->serialWrite("e\n", 2, 1) != 2){
			return false;
		};
		
		// full check of reply due to the possibility of additional warning in it
		{
			// read reply
			int ires = this->serial.readNc(buffer, 150);
			if(ires < 0){
				return false;
			};
			size_t res = (size_t) ires;// for type-safe comparison
			
			// test reply for warning
			char* buff = buffer;
			
			size_t length = ::strlen(REPLY_PULLUPS_SOURCE);
			size_t length_warning = ::strlen(REPLY_PULLUPS_SOURCE_WARNING);
			if(res >= length + length_warning && !::strncmp(buffer, REPLY_PULLUPS_SOURCE_WARNING, length_warning)){// warning found, skip it in next check
				buff += length_warning;
			};
			
			// test reply
			if(res < length || ::strncmp(buff, REPLY_PULLUPS_SOURCE, length)){// strncmp returns 0 when equal
				return false;
			};
			
			// test reply length
			if(res > 150){// check unexpected length
				//TODO log("Something strange happened - BusPirate returned much more than expected, but the checked part is OK.");
				this->serialPurge();
			}
			else if(buffer[res-1] != '>'){// check prompt
				return false;
			};
		}
		
		buffer[0] = '0' + (static_cast<unsigned char>(mode) & 0b111);//TODO support for multi-character numeric option
		buffer[1] = '\n';
		if(this->serialWrite(buffer, 2, 1) != 2){
			return false;
		};
		
		this->serialPurge();
		
		// enable
		if(this->serialWrite("P\n", 2, 1) != 2){
			return false;
		};
		
		if(this->serialReadReply(buffer, 100, REPLY_PULLUPS_ON, true) < 0){
			return false;
		};
	};
	
	return true;
} // setPullUps

bool BusPirateV4::setPSUs(bool enable){
	if(this->serialWrite((enable ? "W\n" : "w\n"), 2, 1) != 2){
		return false;
	};
	
	char buffer[25];
	return (this->serialReadReply(buffer, 25, (enable ? REPLY_PSU_ON : REPLY_PSU_OFF), true) >= 0);
} // setPSUs

void BusPirateV4::serialPurge(){
	byte_t buffer[20];
	while(this->serial.readN(buffer, 20) == 20);
} // serialPurge

int BusPirateV4::serialReadReply(char* buffer, size_t buffer_length, const char* reply, bool prompt_check){
	// read reply
	int ires = this->serial.readNc(buffer, buffer_length);
	if(ires < 0){
		return ires;
	};
	size_t res = (size_t) ires;// for type-safe comparison
	
	// test reply
	size_t length = ::strlen(reply);
	if(res < length || ::strncmp(buffer, reply, length)){// strncmp returns 0 when equal
		return -1;
	};
	
	// test reply length
	if(res > buffer_length){// check unexpected length
		//TODO log("Something strange happened - BusPirate returned much more than expected, but the checked part is OK.");
		this->serialPurge();
	}
	else if(prompt_check && buffer[res-1] != '>'){// check prompt
		return -1;
	};
	
	return ires;
} // serialReadReply

int BusPirateV4::serialWrite(const char* data, unsigned char length, unsigned char read_extra){
	// write data
	int ret = this->serial.writec(data, length);
	if(ret != length){
		return -1;
	};
	
	// "echo" length to read
	if(read_extra > 0){
		length += read_extra;
	};
	
	// flush echo from OS buffer
	char buffer[10];
	unsigned char i = 0;
	while(i < length){
		unsigned char count = length - i;
		if(count > 10){
			count = 10;
		};
		
		if(this->serial.readNc(buffer, count) != count){// not enough echo data
			break;
		};
		
		i += count;
	};
	
	return ret;
} // serialWrite

bool BusPirateV4::checkMode(const char* mode){
	unsigned char length = strlen(mode) + 1;
	char buffer[10];
	
	// check mode length (due to the statically allocated buffer)
	if(length > 10){
		throw ::std::invalid_argument("Mode should not be longer than 10 characters!");
	};
	
	// read mode from serial
	if(this->serial.readNc(buffer, length) != length){
		return false;
	};
	
	return (!(::strncmp(buffer, mode, length-1)) && buffer[length-1] == '>');// strncmp returns 0 when equal
} // checkMode

bool BusPirateV4::switchModeHiZ(){
	// try to switch mode
	if(this->serialWrite("m1\n", 3, 1) != 3){
		return false;
	};
	
	// check success
	return this->switchModeFinalCheck("HiZ");
} // switchModeHiZ

bool BusPirateV4::switchModeI2C(ModeI2C mode){
	// try to switch mode
	if(this->serialWrite("m4\n", 3, 1) != 3){
		return false;
	};
	
	// buffer for reading
	char buffer[70];
	
	// select implementation (SW/HW)
	if(this->serialReadReply(buffer, 70, REPLY_I2C_MODE, true) < 0){
		return false;
	};
	
	buffer[0] = '0' + (static_cast<unsigned char>(mode) >> 6);
	buffer[1] = '\n';
	if(this->serialWrite(buffer, 2, 1) != 2){
		return false;
	};
	
	// select speed
	if(this->serialReadReply(buffer, 70, (((static_cast<unsigned char>(mode) >> 6) == 1) ? REPLY_I2C_SPEEDS_SW : REPLY_I2C_SPEEDS_HW), true) < 0){
		return false;
	};
	
	buffer[0] = '0' + (static_cast<unsigned char>(mode) & 0b111);//TODO support for multi-character numeric option (sprintf, etc.) - 6 bits available
	buffer[1] = '\n';
	if(this->serialWrite(buffer, 2, 1) != 2){
		return false;
	};
	
	// check success
	return this->switchModeFinalCheck("I2C");
} // switchModeI2C

bool BusPirateV4::switchModeFinalCheck(const char* mode){
	// read response
	char buffer[7];
	if(this->serial.readNc(buffer, 7) != 7){// response + new-line
		return false;
	};
	
	// check "Ready" response
	if(::strncmp(buffer, "Ready\r\n", 7)){// strncmp returns 0 when equal
		return false;
	};
	
	// check mode after switching
	return this->checkMode(mode);
} // switchModeFinalCheck

bool BusPirateV4::checkVersion(){
	char buffer[50];
	
	// send request
	if(this->serialWrite("i\n", 2, 1) != 2){
		return false;
	};
	
	if(this->serial.readNc(buffer, 50) < 36){// board + firmware
		return false;
	};
	
	// check response
	bool res = !(::strncmp(buffer, "Bus Pirate v4\r\nFirmware v6.1 r1676", 34));// strncmp returns 0 when equal
	
	// flush other info data from OS buffer
	this->serialPurge();
	
	return res;
} // checkVersion

}}}} // namespace ModuCab::Interfaces::I2C::Drivers
