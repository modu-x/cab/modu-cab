/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Interfaces::I2C::Drivers
 * Linux I2C Driver
 */
#ifndef MODUCAB_INTERFACES_I2C_DRIVERS_LINUX_H_
#define MODUCAB_INTERFACES_I2C_DRIVERS_LINUX_H_


#include <cstdint>

#include "interfaces/i2c/i_driver.h"
#include "utils/types.h"


namespace ModuCab { namespace Interfaces { namespace I2C { namespace Drivers {

class Linux: public ::ModuCab::Interfaces::I2C::IDriver {
	public:
		/**
		 * Constructor
		 * @param bus - number of I2C bus
		 * @param force - whether to try to force using of slave address even when other process is already using it
		 * @param blocking - when false, open device file with O_NONBLOCK flag
		 */
		Linux(unsigned char bus, bool force = false, bool blocking = true);
		
		/**
		 * Constructor
		 * @param fn - file name of special device file belonging to I2C bus
		 * @param force - whether to try to force using of slave address even when other process is already using it
		 * @param blocking - when false, open device file with O_NONBLOCK flag
		 */
		Linux(const char* fn, bool force = false, bool blocking = true);
		
		/**
		 * Destructor
		 */
		~Linux();
		
		
		/**
		 * IDriver
		 */
		int read(byte_t addr, byte_t* data, unsigned char length) override;
		int write(byte_t addr, const byte_t* data, unsigned char length) override;
		int bytes(byte_t addr, const byte_t* wdata, unsigned char wlength, byte_t* rdata, unsigned char rlength) override;
		
		
		/**
		 * Returns adapter functionality mask
		 * @return funcs
		 */
		uint32_t getFuncs(){return this->funcs;};
	
	
	private:
		/** Try to force using of slave address even when other process is already using it */
		bool force;
		
		/** Bus device file-descriptor */
		int fd;
		
		/** Adapter functionality mask */
		uint32_t funcs;
		
		
		/**
		 * Opens device file
		 * @param fn - file name of special device file belonging to I2C bus
		 * @param blocking - when false, open device file with O_NONBLOCK flag
		 */
		void open(const char* fn, bool blocking = true);
		
		/**
		 * Closes device file
		 */
		void close();
}; // class Linux

}}}} // namespace ModuCab::Interfaces::I2C::Drivers


#endif
