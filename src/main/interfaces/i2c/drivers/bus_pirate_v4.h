/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Interfaces::I2C::Drivers
 * Dangerous Prototypes - Bus Pirate v4 - I2C tool
 */
#ifndef MODUCAB_INTERFACES_I2C_DRIVERS_BUSPIRATEV4_H_
#define MODUCAB_INTERFACES_I2C_DRIVERS_BUSPIRATEV4_H_


#include <cstring> // size_t

#include "interfaces/i2c/i_driver.h"
#include "interfaces/serial/i_serial.h"
#include "utils/types.h"


namespace ModuCab { namespace Interfaces { namespace I2C { namespace Drivers {

class BusPirateV4: public ::ModuCab::Interfaces::I2C::IDriver {
	public:
		/**
		 * Modes of pull-up resistors
		 */
		enum class PullUps : unsigned char {
			None = 0,
			External = 1,
			Internal3V3 = 2,
			Internal5V = 3
		}; // enum PullUps
		
		/**
		 * Modes (software/hardware) and speed of I2C bus
		 */
		enum class ModeI2C : unsigned char {
			Software_5kHz   = (1 << 6) | 1,
			Software_50kHz  = (1 << 6) | 2,
			Software_100kHz = (1 << 6) | 3,
			Software_400kHz = (1 << 6) | 4,
			
			Hardware_100kHz = (2 << 6) | 1,
			Hardware_400kHz = (2 << 6) | 2,
			Hardware_1MHz   = (2 << 6) | 3
		}; // enum ModeI2C
		
		
		/**
		 * Constructor
		 * @param serial - reference to Serial interface to communicate with BusPirate board
		 * @param mode - I2C mode (software/hardware) and speed
		 * @param pull_ups - mode of pull-up resistors
		 * @param power - power supplies on/off
		 * @param force - try to switch BusPirate mode even when not in HiZ on beginning
		 */
		BusPirateV4(
			::ModuCab::Interfaces::Serial::ISerial& serial,
			ModeI2C mode = ModeI2C::Hardware_100kHz,
			PullUps pull_ups = PullUps::None,
			bool power = false,
			bool force = false
		);
		
		/**
		 * Destructor
		 */
		~BusPirateV4();
		
		
		/**
		 * IDriver
		 */
		int read(byte_t addr, byte_t* data, unsigned char length) override;
		int write(byte_t addr, const byte_t* data, unsigned char length) override;
		int bytes(byte_t addr, const byte_t* wdata, unsigned char wlength, byte_t* rdata, unsigned char rlength) override;
		
		
		/**
		 * Switches pull-up resistors mode (source and enable/disable)
		 * @param mode
		 * @return success/failure
		 */
		bool setPullUps(PullUps mode);
		
		/**
		 * Switches internal power supplies on/off
		 * @param enable - on/off
		 * @return success/failure
		 */
		bool setPSUs(bool enable);
	
	
	private:
		/** Reference to serial bus used for communication with BusPirate board */
		::ModuCab::Interfaces::Serial::ISerial& serial;
		
		
		/**
		 * Sends start condition and address to BusPirate
		 * @param addr - slave address (7-bit)
		 * @param read - true for read transaction, false for write
		 * @return success/failure
		 */
		bool dataFormatStart(byte_t addr, bool read);
		
		/**
		 * Sends stop condition and whole input enter to BusPirate
		 * @return success/failure
		 */
		bool dataFormatStop();
		
		/**
		 * Sends request for bytes to read to BusPirate
		 * @param length - number of bytes to read
		 * @return success/failure
		 */
		bool dataFormatRead(unsigned char length);
		
		/**
		 * Sends bytes to be written to BusPirate
		 * @param data - pointer to array of bytes to write
		 * @param length - number of bytes to write
		 * @return number of bytes successfully sent to BusPirate
		 */
		unsigned char dataFormatWrite(const byte_t* data, unsigned char length);
		
		/**
		 * Parses BusPirate reply for start condition
		 * @param addr - slave address (7-bit) to check
		 * @param read - true for read transaction, false for write
		 * @return success/failure
		 */
		bool dataParseStart(byte_t addr, bool read);
		
		/**
		 * Parses BusPirate reply for stop condition
		 * @return success/failure
		 */
		bool dataParseStop();
		
		/**
		 * Parses BusPirate reply for read transaction and store read data
		 * @param data - pointer to array of bytes to store the read data
		 * @param length - number of bytes to read
		 * @return number of bytes successfully parsed from output
		 */
		unsigned char dataParseRead(byte_t* data, unsigned char length);
		
		/**
		 * Parses BusPirate reply for single read byte
		 * @param data - pointer to byte to store the read data
		 * @return success/failure
		 */
		bool dataParseReadByte(byte_t* data);
		
		/**
		 * Parses BusPirate reply for write transaction
		 * @param data - pointer to array of bytes to check the reply against
		 * @param length - number of bytes written
		 * @return number of bytes successfully parsed from output
		 */
		unsigned char dataParseWrite(const byte_t* data, unsigned char length);
		
		/**
		 * Parses BusPirate reply for single written byte
		 * @param data - byte to check the reply against
		 * @return success/failure
		 */
		bool dataParseWriteByte(const byte_t data);
		
		/**
		 * Reads all available data from serial and purges them (e.g. does not store them anywhere)
		 */
		void serialPurge();
		
		/**
		 * Reads data from serial and checks whether they match expected reply, purges whole reply up to buffer length and checks if the last read character was prompt
		 * @param buffer - buffer to use
		 * @param buffer_length - length of buffer
		 * @param reply - character string to match
		 * @param prompt_check - when true, last read character must be '>' (e.g. the prompt character)
		 * @return positive = number of read bytes, negative = error code; in case of any detected error in the function itself => returns -1
		 */
		int serialReadReply(char* buffer, size_t buffer_length, const char* reply, bool prompt_check = false);
		
		/**
		 * Writes to serial and reads (and purges) BusPirate echo
		 * @param data - characters to write
		 * @param length - length of data to write
		 * @param read_extra - number of extra bytes to read after echo
		 * @return positive = number of characters written, negative = error code; in case of any detected error in the function itself => returns -1
		 */
		int serialWrite(const char* data, unsigned char length, unsigned char read_extra = 0);
		
		/**
		 * Checks if BusPirate is exposing correct prompt
		 * @param mode - identification of desired mode to check
		 * @return prompt OK
		 */
		bool checkMode(const char* mode);
		
		/**
		 * Switches BusPirate to HiZ (High-Impedation) mode
		 * @return success/failure
		 */
		bool switchModeHiZ();
		
		/**
		 * Switches BusPirate to I2C mode
		 * @param mode - I2C mode (software/hardware) and speed
		 * @return success/failure
		 */
		bool switchModeI2C(ModeI2C mode);
		
		/**
		 * Checks if BusPirate replied with "Ready" and exposed correct prompt after switching mode
		 * @param mode - identification of desired mode to check
		 * @return replies OK
		 */
		bool switchModeFinalCheck(const char* mode);
		
		/**
		 * Returns if BusPirate FW version is supported
		 * @return supported or not
		 */
		bool checkVersion();
}; // class BusPirateV4

}}}} // namespace ModuCab::Interfaces::I2C::Drivers


#endif
