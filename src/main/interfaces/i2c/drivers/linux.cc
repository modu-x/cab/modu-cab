/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "interfaces/i2c/drivers/linux.h"

#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <cstdio>


namespace ModuCab { namespace Interfaces { namespace I2C { namespace Drivers {

Linux::Linux(unsigned char bus, bool force, bool blocking): force(force) {
	// build standard device file name
	char fn[] = "/dev/i2c-\0\0\0";
	
	::snprintf(fn+9, 4, "%d", bus);//TODO error checking
	
	// open it
	this->open(fn, blocking);
} // constructor(unsigned char, bool)

Linux::Linux(const char* fn, bool force, bool blocking): force(force) {
	this->open(fn, blocking);
} // constructor(char*, bool)

Linux::~Linux(){
	this->close();
} // destructor

void Linux::open(const char* fn, bool blocking){
	// check file-name string pointer
	if(!fn){
		//TODO critical error - throw exception?
	};
	
	// prepare flags
	int flags = O_RDWR;
	if(!blocking){
		flags |= O_NONBLOCK;
	};
	
	// open device
	int fd = ::open(fn, flags);
	if(fd < 0){
		//TODO critical error - throw exception?
	};
	this->fd = fd;
	
	// get adapter functionality
	int ret = ::ioctl(this->fd, I2C_FUNCS, &this->funcs);
	if(ret < 0){
		//TODO error - log
	};
} // open

void Linux::close(){
	int fd = this->fd;
	this->fd = -1;
	::close(fd);
} // close

int Linux::read(byte_t addr, byte_t* data, unsigned char length){
	return this->bytes(addr, nullptr, 0, data, length);
} // read

int Linux::write(byte_t addr, const byte_t* data, unsigned char length){
	return this->bytes(addr, data, length, nullptr, 0);
} // write

int Linux::bytes(byte_t addr, const byte_t* wdata, unsigned char wlength, byte_t* rdata, unsigned char rlength){
	// check lengths
	if(!(wlength && wdata) && !(rlength && rdata)){// no data
		return -1;//TODO
	}
	else if(wlength > I2C_SMBUS_BLOCK_MAX || rlength > I2C_SMBUS_BLOCK_MAX){// data too long
		return -1;//TODO
	};
	
	// prepare transactions
	struct i2c_msg msgs[2];
	unsigned char i = 0;
	
	// write part
	if(wlength && wdata){
		msgs[i].addr = addr;
		msgs[i].flags = 0;// write
		msgs[i].len = wlength;
		msgs[i].buf = (__u8*) wdata;//TODO
		i++;
	};
	
	// read part
	if(rlength && rdata){
		msgs[i].addr = addr;
		msgs[i].flags = I2C_M_RD;
		msgs[i].len = rlength;
		msgs[i].buf = rdata;
		i++;
	};
	
	// prepare needed structures
	struct i2c_rdwr_ioctl_data data;
	data.msgs = msgs;
	data.nmsgs = i;
	
	// process
	int ret = ::ioctl(this->fd, I2C_RDWR, &data);
	if(ret < 0){
		return -1;//TODO
	};
	
	return (rlength) ? data.msgs[i-1].len : 0;
} // bytes

}}}} // namespace ModuCab::Interfaces::I2C::Drivers
