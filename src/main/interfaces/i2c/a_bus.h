/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Interfaces::I2C
 * Abstract I2C Bus - handles specific IBus length shortcuts
 */
#ifndef MODUCAB_INTERFACES_I2C_ABUS_H_
#define MODUCAB_INTERFACES_I2C_ABUS_H_


#include "interfaces/i2c/i_bus.h"
#include "utils/types.h"


namespace ModuCab { namespace Interfaces { namespace I2C {

class ABus: public IBus {
	public:
		inline int write_8(const byte_t addr, const byte_t data) override {
			return this->write(addr, &data, 1);
		} // write_8
		
		inline int write_16(const byte_t addr, const byte_t data0, const byte_t data1) override {
			byte_t data[] = {data0, data1};
			return this->write(addr, data, 2);
		} // write_16
		
		inline int write_24(const byte_t addr, const byte_t data0, const byte_t data1, const byte_t data2) override {
			byte_t data[] = {data0, data1, data2};
			return this->write(addr, data, 3);
		} // write_24
		
		inline int write_32(const byte_t addr, const byte_t data0, const byte_t data1, const byte_t data2, const byte_t data3) override {
			byte_t data[] = {data0, data1, data2, data3};
			return this->write(addr, data, 4);
		} // write_32
		
		inline int bytes_8(const byte_t addr, const byte_t wdata, byte_t* rdata, const unsigned char rlength) override {
			return this->bytes(addr, &wdata, 1, rdata, rlength);
		} // bytes_8
		
		inline int bytes_16(const byte_t addr, const byte_t wdata0, const byte_t wdata1, byte_t* rdata, const unsigned char rlength) override {
			byte_t wdata[] = {wdata0, wdata1};
			return this->bytes(addr, wdata, 2, rdata, rlength);
		} // bytes_16
		
		inline int bytes_24(const byte_t addr, const byte_t wdata0, const byte_t wdata1, const byte_t wdata2, byte_t* rdata, const unsigned char rlength) override {
			byte_t wdata[] = {wdata0, wdata1, wdata2};
			return this->bytes(addr, wdata, 3, rdata, rlength);
		} // bytes_24
		
		inline int bytes_32(const byte_t addr, const byte_t wdata0, const byte_t wdata1, const byte_t wdata2, const byte_t wdata3, byte_t* rdata, const unsigned char rlength) override {
			byte_t wdata[] = {wdata0, wdata1, wdata2, wdata3};
			return this->bytes(addr, wdata, 4, rdata, rlength);
		} // bytes_32
}; // class ABus

}}} // namespace ModuCab::Interfaces::I2C


#endif
