/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Interfaces::I2C
 * I2C Driver class interface
 */
#ifndef MODUCAB_INTERFACES_I2C_IDRIVER_H_
#define MODUCAB_INTERFACES_I2C_IDRIVER_H_


#include "utils/types.h"


namespace ModuCab { namespace Interfaces { namespace I2C {

class IDriver {
	public:
		/** Error codes */
		static const int BAD_ADDRESS_OR_BROADCAST_NOT_ALLOWED = -1;
		
		
		/**
		 * Reads data from I2C slave
		 * @param addr - slave address (7-bit)
		 * @param data - pointer to array of bytes to store the read data
		 * @param length - number of bytes to read
		 * @return positive = number of read bytes, negative = error code
		 */
		virtual int read(byte_t addr, byte_t* data, unsigned char length) =0;
		
		/**
		 * Writes data to I2C slave
		 * @param addr - slave address (7-bit)
		 * @param data - pointer to array of bytes to write
		 * @param length - number of bytes to write
		 * @return positive = number of bytes written, negative = error code
		 */
		virtual int write(byte_t addr, const byte_t* data, unsigned char length) =0;
		
		/**
		 * Transaction with I2C slave consisting of write followed by read separated by repeated start bit (no stop bit between)
		 * @param addr - slave address (7-bit)
		 * @param wdata - pointer to array of bytes to write
		 * @param wlength - number of bytes to write
		 * @param rdata - pointer to array of bytes to store the read data
		 * @param rlength - number of bytes to read
		 * @return positive = number of read bytes, negative = error code
		 */
		virtual int bytes(byte_t addr, const byte_t* wdata, unsigned char wlength, byte_t* rdata, unsigned char rlength) =0;
}; // class IDriver

}}} // namespace ModuCab::Interfaces::I2C


#endif
