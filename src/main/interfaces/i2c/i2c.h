/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Interfaces::I2C
 * I2C Bus base class
 */
#ifndef MODUCAB_INTERFACES_I2C_I2C_H_
#define MODUCAB_INTERFACES_I2C_I2C_H_


#include "interfaces/i2c/a_bus.h"
#include "interfaces/i2c/i_driver.h"
#include "utils/types.h"


namespace ModuCab { namespace Interfaces { namespace I2C {

class I2C: public ABus {
	public:
		/**
		 * Constructor
		 * @param driver - I2C driver
		 * @param broadcast - allow general call
		 */
		I2C(IDriver& driver, bool broadcast = false);
		
		
		/**
		 * IDriver
		 */
		int read(byte_t addr, byte_t* data, unsigned char length) override;
		int write(byte_t addr, const byte_t* data, unsigned char length) override;
		int bytes(byte_t addr, const byte_t* wdata, unsigned char wlength, byte_t* rdata, unsigned char rlength) override;
	
	
	private:
		/** I2C driver */
		IDriver& driver;
		
		/** Whether general call on address 0 is allowed */
		bool broadcast;
}; // class I2C

}}} // namespace ModuCab::Interfaces::I2C


#endif
