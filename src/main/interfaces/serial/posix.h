/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Interfaces::Serial::Drivers
 * POSIX Serial implementation
 */
#ifndef MODUCAB_INTERFACES_SERIAL_POSIX_H_
#define MODUCAB_INTERFACES_SERIAL_POSIX_H_


#include "interfaces/serial/a_serial.h"


namespace ModuCab { namespace Interfaces { namespace Serial {

class Posix: public ASerial {
	public:
		/**
		 * Baudrates defined by POSIX
		 */
		enum class Baudrate : unsigned int {
			BR0      =      0, // 0 baud (drop DTR signal line)
			BR50     =     50, // 50 baud
			BR75     =     75, // 75 baud
			BR110    =    110, // 110 baud
			BR134    =    134, // 134.5 baud
			BR150    =    150, // 150 baud
			BR200    =    200, // 200 baud
			BR300    =    300, // 300 baud
			BR600    =    600, // 600 baud
			BR1200   =   1200, // 1.2 kbaud
			BR1800   =   1800, // 1.8 kbaud
			BR2400   =   2400, // 2.4 kbaud
			BR4800   =   4800, // 4.8 kbaud
			BR9600   =   9600, // 9.6 kbaud
			BR19200  =  19200, // 19.2 kbaud
			BR38400  =  38400, // 38.4 kbaud
			BR57600  =  57600, // 57.6 kbaud
			BR115200 = 115200, // 115.2 kbaud
			BR230400 = 230400  // 230.4 kbaud
		}; // enum Baudrate
		
		
		/**
		 * Constructor
		 * @param fn - file name of special device file
		 * @param baudrate - baudrate of serial
		 * @param timeout - timeout in seconds for read syscalls; if 0, read returns immediately with 0 bytes read
		 */
		Posix(const char* fn, Baudrate baudrate, unsigned char timeout = 0);
		
		/**
		 * Destructor
		 */
		~Posix();
		
		
		/**
		 * IDriver
		 */
		int read(byte_t* data, unsigned char length) override;
		int write(const byte_t* data, unsigned char length) override;
		unsigned int getBaudrate() override;
		
		
		/**
		 * Returns actual baudrate
		 * @return baudrate
		 */
		Baudrate getBaudrateEnum();
		
		/**
		 * Sets serial baudrate
		 * @param baudrate - new baudrate to set
		 * @return true if changed successfully, false on error
		 */
		bool setBaudrate(Baudrate baudrate);
	
	
	private:
		/** Serial device file-descriptor */
		int fd;
}; // class Posix

}}} // namespace ModuCab::Interfaces::Serial


#endif
