/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "interfaces/serial/posix.h"

#include <fcntl.h> // File control - fcntl, open
#include <termios.h> // POSIX terminal control - struct termios, tcgetattr, tcsetattr, cfgetospeed, cfsetispeed, cfsetospeed
#include <unistd.h> // UNIX standard functions - read, write, close

#include <stdexcept> // C++ standard exceptions


namespace ModuCab { namespace Interfaces { namespace Serial {

/**
 * Returns termios speed define for given Baudrate
 * @param baudrate
 * @return termios define value
 */
speed_t baudrate2termios(Posix::Baudrate baudrate){
	switch(baudrate){
		case Posix::Baudrate::BR0:
			return B0;
		case Posix::Baudrate::BR50:
			return B50;
		case Posix::Baudrate::BR75:
			return B75;
		case Posix::Baudrate::BR110:
			return B110;
		case Posix::Baudrate::BR134:
			return B134;
		case Posix::Baudrate::BR150:
			return B150;
		case Posix::Baudrate::BR200:
			return B200;
		case Posix::Baudrate::BR300:
			return B300;
		case Posix::Baudrate::BR600:
			return B600;
		case Posix::Baudrate::BR1200:
			return B1200;
		case Posix::Baudrate::BR1800:
			return B1800;
		case Posix::Baudrate::BR2400:
			return B2400;
		case Posix::Baudrate::BR4800:
			return B4800;
		case Posix::Baudrate::BR9600:
			return B9600;
		case Posix::Baudrate::BR19200:
			return B19200;
		case Posix::Baudrate::BR38400:
			return B38400;
		case Posix::Baudrate::BR57600:
			return B57600;
		case Posix::Baudrate::BR115200:
			return B115200;
		case Posix::Baudrate::BR230400:
			return B230400;
	};
	
	throw ::std::logic_error("Missing case in Baudrate switch!");
} // baudrate2termios

/**
 * Returns termios speed define for given Baudrate
 * @param baudrate - termios define value
 * @return enum member
 */
Posix::Baudrate termios2baudrate(speed_t baudrate){
	switch(baudrate){
		case B0:
			return Posix::Baudrate::BR0;
		case B50:
			return Posix::Baudrate::BR50;
		case B75:
			return Posix::Baudrate::BR75;
		case B110:
			return Posix::Baudrate::BR110;
		case B134:
			return Posix::Baudrate::BR134;
		case B150:
			return Posix::Baudrate::BR150;
		case B200:
			return Posix::Baudrate::BR200;
		case B300:
			return Posix::Baudrate::BR300;
		case B600:
			return Posix::Baudrate::BR600;
		case B1200:
			return Posix::Baudrate::BR1200;
		case B1800:
			return Posix::Baudrate::BR1800;
		case B2400:
			return Posix::Baudrate::BR2400;
		case B4800:
			return Posix::Baudrate::BR4800;
		case B9600:
			return Posix::Baudrate::BR9600;
		case B19200:
			return Posix::Baudrate::BR19200;
		case B38400:
			return Posix::Baudrate::BR38400;
		case B57600:
			return Posix::Baudrate::BR57600;
		case B115200:
			return Posix::Baudrate::BR115200;
		case B230400:
			return Posix::Baudrate::BR230400;
	};
	
	throw ::std::logic_error("Missing case in Baudrate switch!");
} // termios2baudrate


Posix::Posix(const char* fn, Baudrate baudrate, unsigned char timeout){
	// check file-name string pointer
	if(!fn){
		throw ::std::invalid_argument("Bad file-name pointer!");
	};
	
	// open device
	int fd = ::open(fn, O_RDWR | O_NOCTTY | O_NDELAY);// read-write access, not a controlling terminal, don't care DCD signal line
	if(fd < 0){
		throw ::std::runtime_error("Could not open given file!");
	};
	
	// whether to use timeout for read syscalls or return 0 read bytes immediately
	fcntl(fd, F_SETFL, ((timeout == 0) ? FNDELAY : 0));
	
	// get initial options
	struct termios options;
	if(::tcgetattr(fd, &options) < 0){
		throw ::std::runtime_error("Failed to get serial port options!");
	};
	
	// set options
	options.c_cflag &= ~CSIZE;// clear size bits
	options.c_cflag |= CS8;// 8-bit size
	
	options.c_cflag &= ~PARENB;// no parity
	options.c_iflag &= ~INPCK;// disable input parity checking
	
	options.c_cflag &= ~CSTOPB;// single stop bit
	
	options.c_cflag &= ~CRTSCTS;// disable hardware flow control
	options.c_iflag &= ~(IXON | IXOFF | IXANY);// disable software flow control
	
	options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);// raw input
	options.c_oflag &= ~OPOST;// raw output
	
	options.c_cc[VMIN]  = 0;// minimum number of bytes to read
	options.c_cc[VTIME] = timeout;// timeout for read syscall
	
	// set the new options
	if(::tcsetattr(fd, TCSANOW, &options) < 0){
		throw ::std::runtime_error("Failed to set new serial port options!");
	};
	
	// save file-descriptor
	this->fd = fd;
	
	// set requested baudrate
	if(!this->setBaudrate(baudrate)){
		this->fd = -1;
		::close(fd);
		throw ::std::runtime_error("Could not set requested baudrate!");
	};
} // constructor

Posix::~Posix(){
	int fd = this->fd;
	this->fd = -1;
	::close(fd);
} // destructor

int Posix::read(byte_t* data, unsigned char length){
	return ::read(this->fd, data, length);
} // read

int Posix::write(const byte_t* data, unsigned char length){
	return ::write(this->fd, data, length);
} // write

unsigned int Posix::getBaudrate(){
	return static_cast<unsigned int>(this->getBaudrateEnum());
} // getBaudrate

Posix::Baudrate Posix::getBaudrateEnum(){
	// get actual options
	struct termios options;
	if(::tcgetattr(this->fd, &options) < 0){
		return Baudrate::BR0;
	};
	
	// get output baud rate (both sould be the same) and convert it
	return termios2baudrate(::cfgetospeed(&options));
} // getBaudrate

bool Posix::setBaudrate(Baudrate baudrate){
	// get actual options
	struct termios options;
	if(::tcgetattr(this->fd, &options) < 0){
		return false;
	};
	
	// select baudrate define
	speed_t rate = baudrate2termios(baudrate);
	
	// set the baud rates
	if(::cfsetispeed(&options, rate) < 0 || ::cfsetospeed(&options, rate) < 0){
		return false;
	};
	
	// set the new options
	if(::tcsetattr(this->fd, TCSANOW, &options) < 0){
		return false;
	};
	
	return true;
} // setBaudrate

}}} // namespace ModuCab::Interfaces::Serial
