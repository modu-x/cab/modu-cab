/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Interfaces::Serial
 * Serial driver interface - the very basic function of serial interface
 */
#ifndef MODUCAB_INTERFACES_SERIAL_IDRIVER_H_
#define MODUCAB_INTERFACES_SERIAL_IDRIVER_H_


#include "utils/types.h"


namespace ModuCab { namespace Interfaces { namespace Serial {

class IDriver {
	public:
		/**
		 * Receives data from Serial
		 * @param data - pointer to array of bytes to store the read data
		 * @param length - number of bytes to read
		 * @return positive = number of read bytes, negative = error code
		 */
		virtual int read(byte_t* data, unsigned char length) =0;
		
		/**
		 * Sends data over Serial
		 * @param data - pointer to array of bytes to send
		 * @param length - number of bytes to send
		 * @return positive = number of bytes sent, negative = error code
		 */
		virtual int write(const byte_t* data, unsigned char length) =0;
		
		/**
		 * Returns actual baudrate
		 * @return baudrate
		 */
		virtual unsigned int getBaudrate() =0;
}; // class IDriver

}}} // namespace ModuCab::Interfaces::Serial


#endif
