/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Interfaces::Serial
 * Abstract Serial
 */
#ifndef MODUCAB_INTERFACES_SERIAL_ASERIAL_H_
#define MODUCAB_INTERFACES_SERIAL_ASERIAL_H_


#include "interfaces/serial/i_serial.h"


namespace ModuCab { namespace Interfaces { namespace Serial {

class ASerial : public ISerial {
	public:
		inline int readc(char* data, unsigned char length) override {
			return this->read((byte_t*) data, length);
		} // readc
		
		inline int writec(const char* data, unsigned char length) override {
			return this->write((const byte_t*) data, length);
		} // writec
		
		inline int readNc(char* data, unsigned char length, unsigned char retries = 3, bool retry_all = false) override {
			return this->readN((byte_t*) data, length, retries, retry_all);
		} // readNc
		
		int readN(byte_t* data, unsigned char length, unsigned char retries = 3, bool retry_all = false) override;
}; // class ASerial

}}} // namespace ModuCab::Interfaces::Serial


#endif
