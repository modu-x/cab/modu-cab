/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "interfaces/serial/a_serial.h"


namespace ModuCab { namespace Interfaces { namespace Serial {

int ASerial::readN(byte_t* data, unsigned char length, unsigned char retries, bool retry_all){
	unsigned char retry = 0;
	for(unsigned char read = 0; read < length;){
		// rest data to read
		unsigned char rest = length - read;
		
		// read it
		int res = this->read(data + read, rest);
		
		if(res < 0){// error
			return res;
		}
		else if(res > rest){// something bad happened - the read command indicates successful read of more bytes than requested
			return -1;
		}
		else {
			// save number of read bytes
			read += res;
			retry++;
			
			// handle retries
			if(res > 0){
				retry = 0;
			}
			else if(retry >= retries || (read > 0 && !retry_all)){
				return read;
			};
		};
	};
	
	return length;
} // readN

}}} // namespace ModuCab::Interfaces::Serial
