/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Interfaces::Serial
 * Serial interface
 */
#ifndef MODUCAB_INTERFACES_SERIAL_ISERIAL_H_
#define MODUCAB_INTERFACES_SERIAL_ISERIAL_H_


#include "interfaces/serial/i_driver.h"
#include "utils/types.h"


namespace ModuCab { namespace Interfaces { namespace Serial {

class ISerial: public IDriver {
	public:
		/**
		 * Receives characters data from Serial
		 * @param data - pointer to array of characters to store the read data
		 * @param length - number of characters to read
		 * @return positive = number of read characters, negative = error code
		 */
		virtual int readc(char* data, unsigned char length) =0;// different name workarounds C++ restriction of overloads resolution across inheritance (scopes)
		
		/**
		 * Sends characters data over Serial
		 * @param data - pointer to array of characters to send
		 * @param length - number of characters to send
		 * @return positive = number of characters sent, negative = error code
		 */
		virtual int writec(const char* data, unsigned char length) =0;// different name workarounds C++ restriction of overloads resolution across inheritance (scopes)
		
		/**
		 * Tries to receive requested number of bytes/characters from Serial - retry the read functions
		 * @param data - pointer to array of bytes/characters to store the read data
		 * @param length - number of bytes to read
		 * @param retries - number of read retries with 0 read bytes before returning with lower than requested number of data bytes
		 * @param retry_all - whether to use retry counter after each 0 length read (always starting from 0) or only for the first one
		 * @return positive = number of read bytes, negative = error code
		 */
		virtual int readN(byte_t* data, unsigned char length, unsigned char retries = 3, bool retry_all = false) =0;
		virtual int readNc(char* data, unsigned char length, unsigned char retries = 3, bool retry_all = false) =0;// different name workarounds C++ restriction of overloads resolution across inheritance (scopes)
}; // class ISerial

}}} // namespace ModuCab::Interfaces::Serial


#endif
