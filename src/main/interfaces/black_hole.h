/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Interfaces
 * Stub-only interface to "close" one side of communication - usable when testing upstream or downstream only with OverCore
 */
#ifndef MODUCAB_INTERFACES_BLACKHOLE_H_
#define MODUCAB_INTERFACES_BLACKHOLE_H_


#include "interfaces/i_generic.h"


namespace ModuCab { namespace Interfaces {

class BlackHole: public ::ModuCab::Interfaces::IGeneric {
	public:
		/**
		 * IGeneric
		 */
		inline int data(const byte_t* data, length_t length, length_t offset) override {return 0;};
		inline int dataRequest(byte_t* data, length_t length, length_t offset) override {return 0;};
		inline int dataRequest(length_t length, length_t offset) override {return 0;};
}; // class BlackHole

}} // namespace ModuCab::Interfaces


#endif
