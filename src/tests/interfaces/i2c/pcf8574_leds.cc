/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "tests/interfaces/i2c/pcf8574_leds.h"

#include <unistd.h> // usleep

#include <stdexcept> // C++ standard exceptions


using ModuCab::Parts::PCF8574;

PCF8574Leds::PCF8574Leds(PCF8574& exp):
	exp(exp)
{
	// test expander mode
	PCF8574::Mode mode = exp.getMode();
	if(mode != PCF8574::Mode::Output && mode != PCF8574::Mode::InOut){
		throw ::std::invalid_argument("Given expander is not in output mode!");
	};
} // constructor

bool PCF8574Leds::setLeds(const ModuCab::byte_t leds){
	return this->exp.out(~leds);
} // setLeds

bool PCF8574Leds::runSequence(const ModuCab::byte_t* seq, unsigned char seq_length, useconds_t step){
	if(!seq){
		throw ::std::invalid_argument("Bad pointer to sequence!");
	};
	
	for(unsigned char i = 0; i < seq_length; i++){
		if(!this->setLeds(seq[i])){
			return false;
		};
		
		if(step > 0){
			::usleep(step);
		};
	};
	
	return true;
} // runSequence

bool PCF8574Leds::runKnightRider(useconds_t step){
	ModuCab::byte_t seq[] = {
		0b00000001,
		0b00000010,
		0b00000100,
		0b00001000,
		0b00010000,
		0b00100000,
		0b01000000,
		0b10000000,
		0b01000000,
		0b00100000,
		0b00010000,
		0b00001000,
		0b00000100,
		0b00000010
	};
	
	return this->runSequence(
		seq,
		14,
		step
	);
} // runKnightRider

bool PCF8574Leds::runChain(useconds_t step){
	ModuCab::byte_t seq[] = {
		0b00000000,
		0b00000001,
		0b00000011,
		0b00000111,
		0b00001111,
		0b00011111,
		0b00111111,
		0b01111111,
		0b11111111,
		0b11111110,
		0b11111100,
		0b11111000,
		0b11110000,
		0b11100000,
		0b11000000,
		0b10000000
	};
	
	return this->runSequence(
		seq,
		16,
		step
	);
} // runChain

bool PCF8574Leds::run3dot(useconds_t step){
	ModuCab::byte_t seq[] = {
		0b00000000,
		0b00000001,
		0b00000011,
		0b00000111,
		0b00001110,
		0b00011100,
		0b00111000,
		0b01110000,
		0b11100000,
		0b11000000,
		0b10000000
	};
	
	return this->runSequence(
		seq,
		11,
		step
	);
} // run3dot
