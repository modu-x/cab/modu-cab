/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include <unistd.h> // sleep

#include <cstdio> // fprintf, printf
#include <stdexcept> // C++ standard exceptions

#include "tests/interfaces/i2c/pcf8574_leds.h"
#include "tests/testing_tools/serial_debugger.h"

#include "interfaces/i2c/i2c.h"
#include "interfaces/i2c/driver_ts.h"
#include "interfaces/i2c/drivers/bus_pirate_v4.h"
#include "interfaces/serial/posix.h"
#include "parts/pcf8574.h"


#define SEQUENCES_STEPS 100000


using ModuCab::Parts::PCF8574;
using ModuCab::Interfaces::I2C::I2C;
using ModuCab::Interfaces::I2C::DriverTS;
using ModuCab::Interfaces::I2C::Drivers::BusPirateV4;
using ModuCab::Interfaces::Serial::Posix;


int main(int argc, char **argv){
	// check arguments
	if(argc < 2){
		fprintf(stderr, "Missing file-name argument.\n");
		return -1;
	};
	
	// init interface
	Posix serial = {argv[1], Posix::Baudrate::BR115200, 3};
	printf("Serial UP\n");
	
	// init debugger
	SerialDebugger debugger = {serial};
	printf("Debugger hooked up\n");
	
	// init BusPirate into I2C mode
	BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Hardware_100kHz, BusPirateV4::PullUps::Internal5V};
	printf("\nBP constructed\n\n");
	
	// wrap it to thread-safe driver container
	DriverTS driver = {bp};
	
	// init I2C bus
	I2C bus = {driver, true};
	
	// init PCF8574
	PCF8574 expander = {bus, 0x38, PCF8574::Mode::Output};
	PCF8574Leds exp = {expander};
	
	// blink all LEDs
	for(unsigned char i = 3; i > 0; i--){
		if(!exp.setLeds(0xFF)){
			return -1;
		};
		sleep(i);
		
		if(!exp.setLeds(0x00)){
			return -1;
		};
		sleep(i);
	};
	
	// run sequences
	for(unsigned char i = 0; i < 5; i++){
		if(!exp.runKnightRider(SEQUENCES_STEPS)){
			return -2;
		};
	};
	
	for(unsigned char i = 0; i < 5; i++){
		if(!exp.runChain(SEQUENCES_STEPS)){
			return -3;
		};
	};
	
	for(unsigned char i = 0; i < 5; i++){
		if(!exp.run3dot(SEQUENCES_STEPS)){
			return -4;
		};
	};
	
	return 0;
} // main
