/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include <unistd.h> // usleep

#include <cstdio> // fprintf, printf
#include <stdexcept> // C++ standard exceptions

#include "tests/testing_tools/serial_debugger.h"

#include "interfaces/i2c/i2c.h"
#include "interfaces/i2c/drivers/bus_pirate_v4.h"
#include "interfaces/serial/posix.h"
#include "parts/pcf8574.h"


#define INTERVAL 100000


using ModuCab::Parts::PCF8574;
using ModuCab::Interfaces::I2C::I2C;
using ModuCab::Interfaces::I2C::Drivers::BusPirateV4;
using ModuCab::Interfaces::Serial::Posix;


int main(int argc, char **argv){
	// check arguments
	if(argc < 2){
		fprintf(stderr, "Missing file-name argument.\n");
		return -1;
	};
	
	// init interface
	Posix serial = {argv[1], Posix::Baudrate::BR115200, 3};
	printf("Serial UP\n");
	
	// init debugger
	SerialDebugger debugger = {serial};
	printf("Debugger hooked up\n");
	
	// init BusPirate into I2C mode
	BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Hardware_100kHz, BusPirateV4::PullUps::Internal5V};
	printf("\nBP constructed\n\n");
	
	// init I2C bus
	I2C bus = {bp, true};
	
	// init PCF8574
	PCF8574 exp = {bus, 0x38, PCF8574::Mode::Input};
	
	// read expander data
	for(unsigned char i = 0; i < 100; i++){
		ModuCab::byte_t states[3];
		
		// test reading of multiple bytes
		if(!exp.in(states, 3)){
			return -1;
		};
		printf("Switches: %02X %02X %02X\n", states[0], states[1], states[2]);
		
		// test reading of single byte
		if(!exp.in(states, 1)){
			return -1;
		};
		printf("Switches: %02X\n", states[0]);
		
		usleep(INTERVAL);
	};
	
	return 0;
} // main
