/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include <unistd.h> // usleep

#include <cstdio> // fprintf, printf
#include <stdexcept> // C++ standard exceptions

#include "tests/testing_tools/serial_debugger.h"

#include "interfaces/i2c/i2c.h"
#include "interfaces/i2c/drivers/bus_pirate_v4.h"
#include "interfaces/serial/posix.h"
#include "parts/pcf8591.h"


#define INTERVAL 10000 // 10 ms


using ModuCab::Parts::PCF8591;
using ModuCab::Interfaces::I2C::I2C;
using ModuCab::Interfaces::I2C::Drivers::BusPirateV4;
using ModuCab::Interfaces::Serial::Posix;


int main(int argc, char **argv){
	// check arguments
	if(argc < 2){
		fprintf(stderr, "Missing file-name argument.\n");
		return -1;
	};
	
	// init interface
	Posix serial = {argv[1], Posix::Baudrate::BR115200, 3};
	printf("Serial UP\n");
	
	// init debugger
	SerialDebugger debugger = {serial};
	printf("Debugger hooked up\n");
	
	// init BusPirate into I2C mode
	BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Hardware_100kHz, BusPirateV4::PullUps::Internal5V};
	printf("\nBP constructed\n\n");
	
	// init I2C bus
	I2C bus = {bp, true};
	
	// init expander
	PCF8591 exp = {bus, 0x48};
	
	// run up and down
	for(unsigned char i = 0; i < 255; i++){
		exp.dac(true, i);
		::usleep(INTERVAL);
	};
	for(unsigned char i = 255; i > 0; i--){
		exp.dac(true, i);
		::usleep(INTERVAL);
	};
	exp.dac(false, 0);
	
	return 0;
} // main
