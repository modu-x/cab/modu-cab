/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include <cstdio> // printf
#include <stdexcept> // C++ standard exceptions

#include "tests/testing_tools/serial_debugger.h"

#include "interfaces/i2c/drivers/bus_pirate_v4.h"
#include "interfaces/serial/i_serial.h"
#include "interfaces/serial/posix.h"


using ::ModuCab::Interfaces::I2C::Drivers::BusPirateV4;
using ::ModuCab::Interfaces::Serial::Posix;


int main(int argc, char **argv){
	// check arguments
	if(argc < 2){
		fprintf(stderr, "Missing file-name argument.\n");
		return -1;
	};
	
	// init interface
	Posix serial = {argv[1], Posix::Baudrate::BR115200, 3};
	printf("Serial UP\n");
	
	// init debugger
	SerialDebugger debugger = {serial};
	printf("Debugger hooked up\n");
	
	// test
	BusPirateV4 bp = {debugger};
	
	//BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Hardware_100kHz};
	//BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Hardware_400kHz};
	//BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Hardware_1MHz};
	//BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Software_5kHz};
	//BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Software_50kHz};
	//BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Software_100kHz};
	//BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Software_400kHz};
	
	//BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Hardware_100kHz, BusPirateV4::PullUps::None};
	//BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Hardware_100kHz, BusPirateV4::PullUps::External};
	//BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Hardware_100kHz, BusPirateV4::PullUps::Internal3V3};
	//BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Hardware_100kHz, BusPirateV4::PullUps::Internal5V};
	
	//BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Hardware_100kHz, BusPirateV4::PullUps::None, true};
	//BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Hardware_100kHz, BusPirateV4::PullUps::External, true};
	
	//BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Hardware_100kHz, BusPirateV4::PullUps::None, true, true};
	
	printf("\nBP constructed\n\n");
	
	return 0;
} // main
