/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * PCF8574 8-bit I2C I/O expander - Leds (output) tests
 */
#ifndef MODUCAB_TESTS_INTERFACES_I2C_PCF8574LEDS_H_
#define MODUCAB_TESTS_INTERFACES_I2C_PCF8574LEDS_H_


#include <unistd.h> // useconds_t

#include "parts/pcf8574.h"
#include "utils/types.h"


class PCF8574Leds {
	public:
		/**
		 * Constructor
		 * @param exp - reference to expander
		 */
		PCF8574Leds(ModuCab::Parts::PCF8574& exp);
		
		
		/**
		 * Sets state of epxander outputs
		 * @param leds - byte of states
		 * @return success/failure (communication)
		 */
		bool setLeds(const ModuCab::byte_t leds);
		
		/**
		 * Sends sequence to expander
		 * @param step - micro-seconds between two sequence steps
		 * @return success/failure (communication)
		 */
		bool runSequence(const ModuCab::byte_t* seq, unsigned char seq_length, useconds_t step);
		
		/**
		 * Does one iteration of Knight Rider sequence
		 * @param step - micro-seconds between two sequence steps
		 * @return success/failure (communication)
		 */
		bool runKnightRider(useconds_t step);
		
		/**
		 * Does one iteration of simple chain sequence
		 * @param step - micro-seconds between two sequence steps
		 * @return success/failure (communication)
		 */
		bool runChain(useconds_t step);
		
		/**
		 * Does one iteration of 3-dot sequence
		 * @param step - micro-seconds between two sequence steps
		 * @return success/failure (communication)
		 */
		bool run3dot(useconds_t step);
	
	
	private:
		/** Reference to expander */
		ModuCab::Parts::PCF8574& exp;
}; // class PCF8574Leds


#endif
