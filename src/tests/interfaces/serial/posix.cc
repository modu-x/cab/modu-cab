/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include <cstdio> // printf
#include <stdexcept> // C++ standard exceptions

#include "interfaces/serial/posix.h"


using ::ModuCab::Interfaces::Serial::Posix;


int main(int argc, char **argv){
	// check arguments
	if(argc < 2){
		fprintf(stderr, "Missing file-name argument.\n");
		return -1;
	}
	else if(argc < 3){
		fprintf(stderr, "Missing baudrate argument.\n");
		return -1;
	};
	
	// parse baudrate
	unsigned int rate;
	if(sscanf(argv[2], "%u", &rate) < 1){
		fprintf(stderr, "Baudrate argument cannot be parsed to unsigned integer.\n");
		return -2;
	};
	
	// convert to enum
	Posix::Baudrate baudrate = static_cast<Posix::Baudrate>(rate);//TODO static_cast does no check at runtime!!!
	
	// init interface
	Posix serial = {argv[1], baudrate, 3};
	
	// test write
	int res = serial.writec("ABCD-1234-abcd\r\n", 16);
	if(res < 0){
		fprintf(stderr, "Failed to write to serial - error code %d.\n", res);
	}
	else {
		printf("Write to serial - from %u bytes %d written.\n", 16, res);
	};
	
	// test read
	char buffer[21];
	res = serial.readNc(buffer, 20);
	if(res < 0){
		fprintf(stderr, "Failed to read from serial - error code %d.\n", res);
	}
	else if(res > 20){
		fprintf(stderr, "Something bad happened - the read command indicates successful read of more bytes than requested!");
	}
	else {
		printf("Read from serial - from %u bytes %d read.\n", 20, res);
		if(res > 0){
			buffer[res] = '\0';
			printf("Data:\n%s\n", buffer);
		};
	};
	
	return 0;
} // main
