/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "tests/testing_tools/serial_debugger.h"

#include <cstdio> // fprintf
#include <stdexcept> // C++ standard exceptions


SerialDebugger::SerialDebugger(::ModuCab::Interfaces::Serial::ISerial& serial, FILE* stream):
	serial(serial)
{
	if(!stream){
		throw ::std::invalid_argument("Bad stream!");
	};
	this->stream = stream;
} // constructor

unsigned int SerialDebugger::getBaudrate(){
	fprintf(this->stream, "SerialDebugger::getBaudrate()...\n");
	unsigned int baudrate = this->serial.getBaudrate();
	fprintf(this->stream, "  > RETURN = %u\n", baudrate);
	return baudrate;
} // getBaudrate

int SerialDebugger::read(ModuCab::byte_t* data, unsigned char length){
	fprintf(this->stream, "SerialDebugger::read(*, %u)...\n", length);
	
	int ret = this->serial.read(data, length);
	this->printRet(ret);
	this->printDataHex((const char*) data, ret);
	
	return ret;
} // read

int SerialDebugger::readc(char* data, unsigned char length){
	fprintf(this->stream, "SerialDebugger::readc(*, %u)...\n", length);
	
	int ret = this->serial.readc(data, length);
	this->printRet(ret);
	this->printData(data, ret);
	
	return ret;
} // readc

int SerialDebugger::readN(ModuCab::byte_t* data, unsigned char length, unsigned char retries, bool retry_all){
	fprintf(this->stream, "SerialDebugger::readN(*, %u, %u, %u)...\n", length, retries, retry_all);
	
	int ret = this->serial.readN(data, length, retries, retry_all);
	this->printRet(ret);
	this->printDataHex((const char*) data, ret);
	
	return ret;
} // readN
int SerialDebugger::readNc(char* data, unsigned char length, unsigned char retries, bool retry_all){
	fprintf(this->stream, "SerialDebugger::readNc(*, %u, %u, %u)...\n", length, retries, retry_all);
	
	int ret = this->serial.readNc(data, length, retries, retry_all);
	this->printRet(ret);
	this->printData(data, ret);
	
	return ret;
} // readNc

int SerialDebugger::write(const ModuCab::byte_t* data, unsigned char length){
	fprintf(this->stream, "SerialDebugger::write(*, %u)...\n", length);
	this->printDataHex((const char*) data, length);
	
	int ret = this->serial.write(data, length);
	this->printRet(ret);
	
	return ret;
} // write

int SerialDebugger::writec(const char* data, unsigned char length){
	fprintf(this->stream, "SerialDebugger::writec(*, %u)...\n", length);
	this->printData(data, length);
	
	int ret = this->serial.writec(data, length);
	this->printRet(ret);
	
	return ret;
} // writec

void SerialDebugger::printRet(int ret){
	fprintf(this->stream, "  > RETURN = %d\n", ret);
} // printRet

void SerialDebugger::printDataChar(const char* data, int length){
	if(length > 0){
		fprintf(this->stream, "  > DATA-CHAR = '%.*s'\n", length, data);
	};
} // printDataChar

void SerialDebugger::printDataHex(const char* data, int length){
	if(length > 0){
		fprintf(this->stream, "  > DATA-HEX =");
		
		for(int i = 0; i < length; i++){
			fprintf(this->stream, " %02X", data[i]);
		};
		
		fprintf(this->stream, "\n");
	};
} // printDataHex

void SerialDebugger::printData(const char* data, int length){
	this->printDataChar(data, length);
	this->printDataHex(data, length);
} // printData
