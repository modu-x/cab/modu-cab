/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * Serial Debugger
 */
#ifndef MODUCAB_TESTS_TESTINGTOOLS_SERIALDEBUGGER_H_
#define MODUCAB_TESTS_TESTINGTOOLS_SERIALDEBUGGER_H_


#include <cstdio> // FILE

#include "interfaces/serial/i_serial.h"


class SerialDebugger : public ModuCab::Interfaces::Serial::ISerial {
	public:
		/**
		 * Constructor
		 * @param serial - reference to underlying Serial interface to debug communication on
		 * @param stream - debugger output
		 */
		SerialDebugger(ModuCab::Interfaces::Serial::ISerial& serial, FILE* stream = stderr);
		
		
		/**
		 * ISerial
		 */
		unsigned int getBaudrate() override;
		int read(ModuCab::byte_t* data, unsigned char length) override;
		int readc(char* data, unsigned char length) override;
		int readN(ModuCab::byte_t* data, unsigned char length, unsigned char retries = 3, bool retry_all = false) override;
		int readNc(char* data, unsigned char length, unsigned char retries = 3, bool retry_all = false) override;
		int write(const ModuCab::byte_t* data, unsigned char length) override;
		int writec(const char* data, unsigned char length) override;
	
	
	private:
		/** Reference to underlying Serial interface to debug communication on */
		ModuCab::Interfaces::Serial::ISerial& serial;
		
		/** Debugger output */
		FILE* stream;
		
		
		/**
		 * Outputs return value
		 * @param ret - return value to print
		 */
		void printRet(int ret);
		
		/**
		 * Outputs data as characters
		 * @param data - array to print
		 * @param length - length of data
		 */
		void printDataChar(const char* data, int length);
		
		/**
		 * Outputs data as 2 hexadecimal characters
		 * @param data - array to print
		 * @param length - length of data
		 */
		void printDataHex(const char* data, int length);
		
		/**
		 * Outputs data (characters + hexadecimal)
		 * @param data - array to print
		 * @param length - length of data
		 */
		void printData(const char* data, int length);
}; // class SerialDebugger


#endif
