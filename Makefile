# ***********************************************
# ***                 ModuCab                 ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


## Directories (source, build/object, binaries/output)
FS=src
FSM=$(FS)/main
FST=$(FS)/tests

FO=build
FB=bin


## Compiler settings
# Compiler binary
CC=g++

# Standard
CFS=-std=c++14

# Flags
CFO=$(CFS) -Wall -Werror -pedantic -c
CFD=$(CFS) -MM

# Start searching for includes in project's top-level source directories
CFFM=-iquote$(FSM)
CFFT=-iquote$(FS) $(CFFM)

# Dependencies shortcut flags
CFDM=$(CFD) $(CFFM)
CFDT=$(CFD) $(CFFT)

# Modules shortcut flags
CFOM=$(CFO) $(CFFM)
CFOT=$(CFO) $(CFFT)


## General targets
# Default target
all: main tests

# Remove all generated files
clean:
	rm -f $(FO)/* $(FB)/*

# Main target
main: \
	$(FO)/core_sequential.o \
	$(FO)/interfaces_i2c_drivers_linux.o \
	$(FO)/over-core_core.o

# Test target
tests: \
	$(FB)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-init \
	$(FB)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-pcf8574-leds \
	$(FB)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-pcf8574-leds-ts \
	$(FB)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-pcf8574-switches \
	$(FB)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-pcf8591-dac \
	$(FB)/tests---interfaces_serial_posix


## Dependencies
# Load all builded dependencies
include $(FO)/*.d

# when no .d file exists, make calls next rule which fails for '*.d' file
#   => due to that, the hack with test before invoking compiler itself is needed
# the hack after compiler removes dependency file and old object file in case of error during dependency generation
#   => otherwise, the bash creates empty dependency file which leads to broken dependencies
#   or even leaving the old object file untouched until invoking the clean target

# General rules for building
$(FO)/%.d:
	test '$*' = '*' \
		|| $(CC) $(CFDM) "$(FSM)/`bash -c 'TMP=\"\$${0//_/\\/}\"; echo \"\$${TMP//-/_}\"' '$*'`.cc" -MT '$$(FO)/$*.o $$(FO)/$*.d' > $@ \
		|| (rm $(FO)/$*.* && exit 1)

$(FO)/tests---%.d:
	$(CC) $(CFDT) "$(FST)/`bash -c 'TMP=\"\$${0//_/\\/}\"; echo \"\$${TMP//-/_}\"' '$*'`.cc" -MT '$$(FO)/tests---$*.o $$(FO)/tests---$*.d' > $@ \
		|| (rm $(FO)/tests---$*.* && exit 1)

# Do not delete the dependency files after first build (as this violates their purpose)
.PRECIOUS: \
	$(FO)/%.d \
	$(FO)/tests---%.d


## Modules - compilations
# Main modules
$(FO)/interfaces_i2c_drivers_linux.o: $(FO)/interfaces_i2c_drivers_linux.d
	@echo '### For Linux I2C driver, the system header files <linux/i2c.h> and <linux/i2c-dev.h> are needed. For Debian, these could be obtained by installing the package "libi2c-dev".'
	$(CC) $(CFOM) $(FSM)/interfaces/i2c/drivers/linux.cc -o $(FO)/interfaces_i2c_drivers_linux.o

# Test modules
$(FO)/tests---interfaces_i2c_pcf8574-leds.o: $(FO)/tests---interfaces_i2c_pcf8574-leds.d
	$(CC) -o $@ $(CFOT) -D_GNU_SOURCE $(FST)/interfaces/i2c/pcf8574_leds.cc

$(FO)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-pcf8574-switches.o: $(FO)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-pcf8574-switches.d
	$(CC) -o $@ $(CFOT) -D_GNU_SOURCE $(FST)/interfaces/i2c/drivers/bus_pirate_v4/manual_pcf8574_switches.cc

# General rules for compiling (all modules targets must always depend on corresponding .d dependency file, otherwise it is not builded, so not working at all)
$(FO)/%.o: $(FO)/%.d
	$(CC) -o $@ $(CFOM) "$(FSM)/`bash -c 'TMP=\"\$${0//_/\\/}\"; echo \"\$${TMP//-/_}\"' '$*'`.cc"

$(FO)/tests---%.o: $(FO)/tests---%.d
	$(CC) -o $@ $(CFOT) "$(FST)/`bash -c 'TMP=\"\$${0//_/\\/}\"; echo \"\$${TMP//-/_}\"' '$*'`.cc"


## Binaries - building
# Tests
$(FB)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-init: $(FO)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-init.o \
		$(FO)/tests---testing-tools_serial-debugger.o \
		$(FO)/interfaces_i2c_drivers_bus-pirate-v4.o \
		$(FO)/interfaces_serial_a-serial.o \
		$(FO)/interfaces_serial_posix.o
	$(CC) -o $@ $^

$(FB)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-pcf8574-leds: $(FO)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-pcf8574-leds.o \
		$(FO)/tests---interfaces_i2c_pcf8574-leds.o \
		$(FO)/tests---testing-tools_serial-debugger.o \
		$(FO)/interfaces_i2c_i2c.o \
		$(FO)/interfaces_i2c_drivers_bus-pirate-v4.o \
		$(FO)/interfaces_serial_a-serial.o \
		$(FO)/interfaces_serial_posix.o \
		$(FO)/parts_pcf8574.o
	$(CC) -o $@ $^

$(FB)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-pcf8574-leds-ts: $(FO)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-pcf8574-leds-ts.o \
		$(FO)/tests---interfaces_i2c_pcf8574-leds.o \
		$(FO)/tests---testing-tools_serial-debugger.o \
		$(FO)/interfaces_i2c_i2c.o \
		$(FO)/interfaces_i2c_driver-ts.o \
		$(FO)/interfaces_i2c_drivers_bus-pirate-v4.o \
		$(FO)/interfaces_serial_a-serial.o \
		$(FO)/interfaces_serial_posix.o \
		$(FO)/parts_pcf8574.o
	$(CC) -o $@ $^

$(FB)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-pcf8574-switches: $(FO)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-pcf8574-switches.o \
		$(FO)/tests---testing-tools_serial-debugger.o \
		$(FO)/interfaces_i2c_i2c.o \
		$(FO)/interfaces_i2c_drivers_bus-pirate-v4.o \
		$(FO)/interfaces_serial_a-serial.o \
		$(FO)/interfaces_serial_posix.o \
		$(FO)/parts_pcf8574.o
	$(CC) -o $@ $^

$(FB)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-pcf8591-dac: $(FO)/tests---interfaces_i2c_drivers_bus-pirate-v4_manual-pcf8591-dac.o \
		$(FO)/tests---testing-tools_serial-debugger.o \
		$(FO)/interfaces_i2c_i2c.o \
		$(FO)/interfaces_i2c_drivers_bus-pirate-v4.o \
		$(FO)/interfaces_serial_a-serial.o \
		$(FO)/interfaces_serial_posix.o \
		$(FO)/parts_pcf8591.o
	$(CC) -o $@ $^

$(FB)/tests---interfaces_serial_posix: $(FO)/tests---interfaces_serial_posix.o \
		$(FO)/interfaces_serial_a-serial.o \
		$(FO)/interfaces_serial_posix.o
	$(CC) -o $@ $^
